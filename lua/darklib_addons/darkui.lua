--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
DarkUI = DarkUI or {}
DarkUI.Name = "Darkron GUI"
DarkUI.Description = "DarkLib Derma stuff and helper"
DarkUI.Author = "JustPlayer"
DarkUI.Loaded = false

-- Deprecated
function DarkUI.CreateFont(name, size, font, weight, anti, outline)
    if SERVER then return end
    DarkLib:LogDebug("DarkUI.CreateFont is deprecatd. please use DarkLib.CreateFont (or DarkLib.CreateFontEx) instead.")

    local data = {
        font = font,
        size = size and DarkLib.Scale(size) or nil,
        weight = weight,
        antialias = anti,
        outline = outline
    }

    DarkLib.CreateFontEx(name, data)
end

--[[
    Default Colors
    Im totally not using rgb to enable the color feature of vscode
--]]
local rgb, _C = Color, DarkLib.ModifyColor
local TextColor = color_white -- One color to rule them all

DarkUI.Theme = {
    Primary = rgb(36, 38, 38),
    Background = rgb(54, 54, 58),
    Accent = rgb(92, 103, 125),
    Red = rgb(230, 58, 64),
    Green = rgb(46, 204, 113),
    Blue = rgb(41, 128, 185),
    Text = {
        Default = TextColor,
        Brighter = _C(TextColor, 1.3),
        Bright = _C(TextColor, 1.6),
        Darker = _C(TextColor, 0.6),
        Dark = _C(TextColor, 0.3)
    }
}

if CLIENT then
    --[[
        Default Fonts
    --]]
    DarkLib.CreateFont("FrameButton", 24, "Montserrat", 900)
    DarkLib.CreateFont("FrameTitle", 20, "Roboto", 500)
    DarkLib.CreateFont("DarkUI.FrameTitleBig", 28, "Roboto", 600)
    DarkLib.CreateFont("TextSmall", 14, "Roboto", 400)
    DarkLib.CreateFont("Text", 18, "Roboto", 400)
    DarkLib.CreateFont("TextLarge", 20, "Roboto", 400)
    DarkLib.CreateFont("OverheadText", 120, "Roboto", 600)
end

--[[
    Deprecated stuff
--]]
DarkUI.Classes = {}
local EMPTY_FUNC = function() end

DarkUI.RegisterClass = function(name, data)
    DarkUI.Classes[name] = data
end

function DarkUI.Setup(class, panel, parent)
    local class = DarkUI.Classes[class] or {}
    class.SetupClass = (class.SetupClass and isfunction(class.SetupClass) and class.SetupClass) or function() return end

    for i, k in pairs(class) do
        panel[i] = k
    end

    panel.ClearPaint = function(self)
        self.Paint = EMPTY_FUNC
    end

    return panel:SetupClass() or panel
end

function DarkUI.Create(classname, parent, name)
    return DarkUI.Setup(classname, vgui.Create(classname, parent, name), parent)
end

--[[
    Other setup
--]]
function DarkUI.Load()
    DarkLib.LoadDirectory(DarkUI.FileName .. "/vgui/", DarkLib.IncludeCL)
    DarkLib.LoadDirectory(DarkUI.FileName .. "/vgui/deprecated/", DarkLib.IncludeCL) -- Elements in here will be removed later.
    DarkLib.LoadDirectory(DarkUI.FileName .. "/elements/", DarkLib.IncludeCL) -- Complete re-implementations of something or fully custom
end

hook.Add("OnScreenSizeChanged", "DARKLIB:ResetFonts", function()
    DarkLib.ReloadCurrentLuaFile()
    timer.Simple(0, DarkUI.Load)
    DarkLib:LogNotice("Reload DarkUI")
end)

DarkUI.EnableShadows = GetConVar("darklib_shadows")
DARKUI = DarkUI -- Backwards

return "DarkUI", "DarkUI", {"darklib"}