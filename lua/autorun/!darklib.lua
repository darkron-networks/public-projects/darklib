--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
DarkLib = DarkLib or {}
DarkLib.Name = "Darkron Library"
DarkLib.Description = "Library used for my Addons"
DarkLib.Author = "JustPlayer & Others"
DarkLib.Version = "dev"
DarkLib.IncludeSV = SERVER and include or function() end
DarkLib.IncludeCL = SERVER and AddCSLuaFile or include
DarkLib.Debug = CreateConVar("darklib_debug", "0", FCVAR_CLIENTCMD_CAN_EXECUTE, "Change Debugmode for Darklib", 0, 1)

DarkLib.IncludeSH = function(file)
    AddCSLuaFile(file)

    return include(file)
end

DarkLib.Loaded = false
file.CreateDir("darklib")
-- Utils
DarkLib.IncludeSH("darklib/utils/thirdparty/promise.lua")
DarkLib.IncludeSV("darklib/utils/thirdparty/mysqlite.lua")
DarkLib.IncludeSH("darklib/utils/network.lua")
DarkLib.IncludeCL("darklib/utils/imgur.lua")
DarkLib.IncludeSH("darklib/utils/thirdparty/md5.lua")
DarkLib.IncludeCL("darklib/utils/thirdparty/basexx.lua")
DarkLib.IncludeCL("darklib/utils/thirdparty/cl_imgui.lua")
DarkLib.IncludeCL("darklib/utils/thirdparty/cl_bshadows.lua")
DarkLib.IncludeCL("darklib/utils/thirdparty/cl_ui3d2d.lua")
DarkLib.IncludeSH("darklib/utils/thirdparty/sh_messagepack.lua")
DarkLib.IncludeCL("darklib/utils/thirdparty/cl_arc.lua")
DarkLib.IncludeCL("darklib/utils/draw.lua")
-- Functions
DarkLib.IncludeSH("darklib/sh_functions.lua")
DarkLib.IncludeSV("darklib/sv_functions.lua")
DarkLib.IncludeCL("darklib/cl_functions.lua")
-- Addons
DarkLib:Log("Loading Addons")
DarkLib.IncludeSH("darklib/sh_addons.lua")
-- Ready
DarkLib.Loaded = true
DarkLib:Log("Fully loaded")
hook.Run("DarkLib:FullyLoaded", "for you bboy")