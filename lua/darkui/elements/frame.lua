--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0

    This DFrame is more based on https://www.youtube.com/watch?v=aChXeLsLtfQ
]]
local Scale, FrameTime = DarkLib.Scale, FrameTime
local LerpColor, BSHADOWS = DarkLib.LerpColor, DarkLib.BSHADOWS
local EnableShadows = DarkUI.EnableShadows
local PANEL = {}
AccessorFunc(PANEL, "m_iCornerRadius", "CornerRadius", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bBodyPadding", "BoddyPadding", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bPaintShadow", "PaintShadow", FORCE_BOOL)
AccessorFunc(PANEL, "m_bBigHeader", "BigHeader", FORCE_BOOL)
AccessorFunc(PANEL, "m_iMinWidth", "MinWidth", FORCE_NUMBER)
AccessorFunc(PANEL, "m_iMinHeight", "MinHeight", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bDeleteOnClose", "DeleteOnClose", FORCE_BOOL)
AccessorFunc(PANEL, "m_bScreenLock", "ScreenLock", FORCE_BOOL)
AccessorFunc(PANEL, "m_bDraggable", "Draggable", FORCE_BOOL)
AccessorFunc(PANEL, "m_bSizable", "Sizable", FORCE_BOOL)
AccessorFunc(PANEL, "m_bNoDraw", "NoDraw", FORCE_BOOL)
AccessorFunc(PANEL, "m_bTransparent", "Transparent", FORCE_BOOL) -- Not supported for now 
-- Backwards
AccessorFunc(PANEL, "m_iCornerRadius", "RoundedCorners", FORCE_NUMBER)

function PANEL:Init()
    self.Theme = DarkUI.GetTheme() -- In case Theme was changed
    -- Default values
    self.m_iCornerRadius = Scale(6)
    self.m_bPaintShadow = true
    self.m_bBigHeader = false
    self.m_bHighQuality = true -- We are fancy here
    self.m_iMinWidth = 100
    self.m_iMinHeight = 50
    self.m_bBodyPadding = self.m_iCornerRadius
    -- Header
    self.header = self:Add("DarkUI_Panel")
    self.header.Margin = 0
    self.header:Dock(TOP)

    -- Header setup
    do
        self.header.Paint = function(pnl, w, h)
            if self.m_bNoDraw then return end
            draw.RoundedBoxEx(self.m_iCornerRadius, 0, 0, w, h, self.Theme.Primary, true, true, false, false)
        end

        self.header.OnMousePressed = function(hdr)
            if self.m_bDraggable then
                self.Dragging = {gui.MouseX() - self.x, gui.MouseY() - self.y}
                hdr:MouseCapture(true)
            end
        end

        self.header.OnMouseReleased = function(hdr)
            self.Dragging = nil
            hdr:MouseCapture(false)
        end

        -- Close button
        self.header.closeBtn = self.header:Add("DButton")
        self.header.closeBtn:Dock(RIGHT)
        self.header.closeBtn:SetText("")
        self.header.closeBtn.CurrentColor = color_white

        self.CloseAction = function(pnl)
            self:Close()
        end

        self.header.closeBtn.DoClick = function(pnl)
            self:CloseAction()
        end

        self.header.closeBtn.Think = function(pnl)
            self.header.closeBtn.TargetColor = pnl.Hovered and self.Theme.Red or color_white

            if self.m_bHighQuality then
                self.header.closeBtn.CurrentColor = LerpColor(FrameTime() * 10, self.header.closeBtn.CurrentColor or color_white, self.header.closeBtn.TargetColor)
            else
                self.header.closeBtn.CurrentColor = self.header.closeBtn.TargetColor
            end
        end

        self.header.closeBtn.Paint = function(pnl, w, h)
            if self.m_bNoDraw then return end
            local margin = self.header.Margin
            surface.SetDrawColor(self.header.closeBtn.CurrentColor)
            surface.SetMaterial(self.ButtonMaterial or DarkLib.Materials.Cross)
            surface.DrawTexturedRect(margin, margin, w - (margin * 2), h - (margin * 2))
        end
    end

    -- Label
    self.header.title = self.header:Add("DarkUI.Label")
    self.header.title:Dock(LEFT)
    self.header.title:SetFont("DarkUI.FrameTitle")
    self:SetTitle("Untitled Window")
    self:SetDeleteOnClose(true)
    -- Body
    self.body = self:Add("DarkUI_Panel")
    self.body:Dock(FILL)
    self.body:DockMargin(self.m_bBodyPadding, 0, self.m_bBodyPadding, self.m_bBodyPadding)

    -- Load Cross
    DarkLib.Images.QueueLoadMaterials({
        image = "HgMhjrI",
        func = function(mat)
            DarkLib.Materials.Cross = mat
        end
    })

    -- Use body now
    function self:Add(...)
        return self.body:Add(...)
    end
end

function PANEL:SetFancy(bool)
    self.m_bHighQuality = bool

    if self.m_bHighQuality then
        self:SetPaintShadow(true)
    else
        self:SetPaintShadow(false)
    end
end

function PANEL:SetTitle(str)
    self.header.title:SetText(str)
    self.header.title:SizeToContents()
end

function PANEL:GetFancy()
    return self.m_bHighQuality
end

function PANEL:GetTitle()
    return self.header.title:GetText()
end

function PANEL:GetBody()
    return self.body
end

function PANEL:PerformLayout()
    if not self.m_bBigHeader then
        self.header.Margin = Scale(8)
        self.m_bHeaderHeight = Scale(32)
        self.header.title:SetFont("DarkUI.FrameTitle")
    else
        self.header.Margin = Scale(16)
        self.m_bHeaderHeight = Scale(48)
        self.header.title:SetFont("DarkUI.DarkUI.FrameTitleBig")
    end

    self.header:SetTall(self.m_bHeaderHeight)

    if IsValid(self.header.closeBtn) then
        self.header.closeBtn:SetWide(self.header:GetTall())
    end

    self.header.title:SizeToContents()
    self.header.title:SetTextInset(self.header.Margin, 0)
end

function PANEL:Paint(w, h)
    if self.m_bNoDraw then return end

    if self.m_bPaintShadow and EnableShadows:GetBool() then
        local aX, aY = self:LocalToScreen()
        BSHADOWS.BeginShadow()
        draw.RoundedBox(self.m_iCornerRadius, aX, aY, w, h, self.Theme.Background)
        BSHADOWS.EndShadow(1, 2, 2)
    else
        draw.RoundedBox(self.m_iCornerRadius, 0, 0, w, h, self.Theme.Background)
    end
end

--[[
    Core DFrame stuff
]]
function PANEL:Close()
    self:SetVisible(false)

    if (self:GetDeleteOnClose()) then
        self:Remove()
    end

    self:OnClose()
end

function PANEL:OnClose()
end

function PANEL:Center()
    self:InvalidateLayout(true)
    self:CenterVertical()
    self:CenterHorizontal()
end

function PANEL:IsActive()
    if (self:HasFocus()) then return true end
    if (vgui.FocusedHasParent(self)) then return true end

    return false
end

function PANEL:Think()
    local mousex = math.Clamp(gui.MouseX(), 1, ScrW() - 1)
    local mousey = math.Clamp(gui.MouseY(), 1, ScrH() - 1)

    if (self.Dragging) then
        local x = mousex - self.Dragging[1]
        local y = mousey - self.Dragging[2]

        -- Lock to screen bounds if screenlock is enabled
        if (self:GetScreenLock()) then
            x = math.Clamp(x, 0, ScrW() - self:GetWide())
            y = math.Clamp(y, 0, ScrH() - self:GetTall())
        end

        self:SetPos(x, y)
    end

    if (self.Sizing) then
        local x = mousex - self.Sizing[1]
        local y = mousey - self.Sizing[2]
        local px, py = self:GetPos()

        if (x < self.m_iMinWidth) then
            x = self.m_iMinWidth
        elseif (x > ScrW() - px and self:GetScreenLock()) then
            x = ScrW() - px
        end

        if (y < self.m_iMinHeight) then
            y = self.m_iMinHeight
        elseif (y > ScrH() - py and self:GetScreenLock()) then
            y = ScrH() - py
        end

        self:SetSize(x, y)
        self:SetCursor("sizenwse")

        return
    end

    local screenX, screenY = self:LocalToScreen(0, 0)

    if (self.Hovered and self.m_bSizable and mousex > (screenX + self:GetWide() - 20) and mousey > (screenY + self:GetTall() - 20)) then
        self:SetCursor("sizenwse")

        return
    end

    self:SetCursor("arrow")

    -- Don't allow the frame to go higher than 0
    if (self.y < 0) then
        self:SetPos(self.x, 0)
    end
end

function PANEL:OnMousePressed()
    local screenX, screenY = self:LocalToScreen(0, 0)

    if (self.m_bSizable and gui.MouseX() > (screenX + self:GetWide() - 20) and gui.MouseY() > (screenY + self:GetTall() - 20)) then
        self.Sizing = {gui.MouseX() - self:GetWide(), gui.MouseY() - self:GetTall()}
        self:MouseCapture(true)

        return
    end
end

function PANEL:OnMouseReleased()
    self.Sizing = nil
    self:MouseCapture(false)
end

derma.DefineControl("DarkUI.Frame", "Advanced DarkUI Window (Can be changed anytime)", PANEL, "EditablePanel")
derma.DefineControl("DarkUI_FrameAdv", "Deprecated", PANEL, "EditablePanel")