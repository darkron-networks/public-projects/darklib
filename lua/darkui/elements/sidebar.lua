--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local _C, ScreenScale = DarkLib.ModifyColor, DarkLib.Scale
--local gradient_t, gradient_b = Material("vgui/gradient_down"), Material("vgui/gradient_up")
--[[
    Sidebar Button
]]
local PANEL = {}

function PANEL:Init()
    self.Theme = DarkUI.GetTheme()
    self:SetContentAlignment(ScreenScale(4))
    self:SetTextInset(ScreenScale(8), 0)
    self:SetTextColor(self.Theme.Text.Default)
end

function PANEL:Paint(w, h)
    if self.m_bActive then
        local lineHeight = ScreenScale(2)
        surface.SetDrawColor(self.Theme.Accent)
        surface.DrawRect(w - lineHeight, 0, lineHeight, h)
    end
end

function PANEL:SetActive(bool)
    self.m_bActive = bool

    if self.m_bActive then
        self:SetTextColor(self.Theme.Accent)
    else
        self:SetTextColor(self.Theme.Text.Default)
    end
end

function PANEL:SetText(txt)
    self.BaseClass.SetText(self, txt)
    self:SizeToContentsX(24)
end

vgui.Register("DarkUI_SidebarBtn", PANEL, "DarkUI_Button")
--[[
    Navbar
]]
local PANEL = {}
AccessorFunc(PANEL, "m_body", "Body")
AccessorFunc(PANEL, "m_bBig", "Big", FORCE_BOOL)
AccessorFunc(PANEL, "m_fFadeTime", "FadeTime")
AccessorFunc(PANEL, "m_bMaxWith", "MaxWith", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bMinWith", "MinWith", FORCE_NUMBER)
-- Just in case 
AccessorFunc(PANEL, "m_bTopLeftRound", "RoundTopLeft", FORCE_BOOL)
AccessorFunc(PANEL, "m_bTopRightRound", "RoundTopRight", FORCE_BOOL)
AccessorFunc(PANEL, "m_bBottomLeftRound", "RoundBottomLeft", FORCE_BOOL)
AccessorFunc(PANEL, "m_bBottomRightRound", "RoundBottomRight", FORCE_BOOL)
AccessorFunc(PANEL, "m_bFillParent", "FillParent", FORCE_BOOL)
AccessorFunc(PANEL, "m_iCornerRadius", "CornerRadius", FORCE_NUMBER)
-- Backwards
AccessorFunc(PANEL, "m_iCornerRadius", "RoundCorner", FORCE_NUMBER)
AccessorFunc(PANEL, "m_iCornerRadius", "RoundedCorners", FORCE_NUMBER)

function PANEL:Init()
    self.Theme = DarkUI.GetTheme()
    -- Defaults
    self.m_bBottomLeftRound = true
    self.m_bMinWith = ScreenScale(120)
    self.m_bMaxWith = ScreenScale(240)
    self.buttons = {}
    self.panels = {}
    -- Setup
    self:Dock(LEFT)
    self:SetWide(ScreenScale(130))
    self.VBar:SetWide(0)
end

function PANEL:OnItemAdded(id, button, panel)
end

function PANEL:AddTab(name, pnl)
    -- Button
    local i = #self.buttons + 1
    self.buttons[i] = self:Add("DarkUI_SidebarBtn")
    local btn = self.buttons[i]
    btn.id = i
    btn:Dock(TOP)
    btn:SetText(name)

    btn.DoClick = function(pnl)
        self:SetActive(pnl.id)
    end

    --self.tabScroller:AddPanel(btn) -- TODO: own implementation?
    -- Panel
    self.panels[i] = self:GetBody():Add(pnl or "DPanel")
    local panel = self.panels[i]
    panel:Dock(FILL)
    panel:SetVisible(false)
    self:OnItemAdded(btn.id, btn, panel)

    return panel
end

function PANEL:OnActiveChange(old, new)
end

function PANEL:ScrollToChild(panel)
    --  self.tabScroller:ScrollToChild(panel)
end

function PANEL:SetActive(id)
    if self.active == id then return end
    local btn = self.buttons[id]
    local panel = self.panels[id]
    if not IsValid(btn) then return end
    if not IsValid(panel) then return end
    -- Button
    local activeBtn = self.buttons[self.active]

    if IsValid(activeBtn) then
        activeBtn:SetActive(false)
    end

    -- Panel
    --self.animFade:Start(self:GetFadeTime(), {
    --    OldTab = self.panels[self.active],
    --    NewTab = panel
    --})
    local activePnl = self.panels[self.active]

    if IsValid(activePnl) then
        activePnl:SetVisible(false)
    end

    self.active = id
    btn:SetActive(true)
    panel:SetVisible(true)
    self:OnActiveChange(activeBtn and activeBtn.id or false, id)
end

function PANEL:SetActiveName(name, force)
    for i = 1, #self.buttons do
        local btn = self.buttons[i]
        if not IsValid(btn) then continue end

        if force then
            if string.StartWith(btn:GetText(), name) then
                self:SetActive(btn.id)
                self:ScrollToChild(btn)
            end
        else
            if btn:GetText() == name then
                self:SetActive(btn.id)
                self:ScrollToChild(btn)
            end
        end
    end
end

function PANEL:PerformLayout(w, h)
    --local ButtonSpace = 1
    local ButtonHeight = 1

    if self.m_bBig then
        ButtonSpace = ScreenScale(24)
        ButtonHeight = ScreenScale(32)
    else
        ButtonSpace = ScreenScale(14)
        ButtonHeight = ScreenScale(22)
    end

    local width = 0

    for i = 1, #self.buttons do
        local btn = self.buttons[i]
        if not IsValid(btn) then continue end
        btn:SizeToContentsY(ButtonHeight)
        if self.m_bFillParent then continue end -- We dont need to resize then
        width = math.max(width, btn:GetWide(), self.m_bMinWith)
    end

    if not self.m_bFillParent then
        self:SetWide(math.min(width, self.m_bMaxWith))
    end

    self.BaseClass.PerformLayout(self, w, h)
end

function PANEL:Paint(w, h)
    if self.m_iCornerRadius then
        draw.RoundedBoxEx(self.m_iCornerRadius, 0, 0, w, h, self.Theme.Primary, self.m_bTopLeftRound, self.m_bTopRightRound, self.m_bBottomLeftRound, self.m_bBottomRightRound)
    else
        surface.SetDrawColor(self.Theme.Primary)
        surface.DrawRect(0, 0, w, h)
    end
end

derma.DefineControl("DarkUI_Sidebar", "Sidebar", PANEL, "DarkUI_ScrollPanel")