--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local Scale = DarkLib.Scale
local GetPhrase = language.GetPhrase
local draw_SimpleText = draw.SimpleText
local string_StartWith, string_sub = string.StartWith, string.sub
local PANEL = {}
AccessorFunc(PANEL, "m_iCornerRadius", "CornerRadius", FORCE_NUMBER)
AccessorFunc(PANEL, "m_cbackgroundColor", "BackgroundColor")
AccessorFunc(PANEL, "m_ctextColor", "TextColor")
AccessorFunc(PANEL, "m_splaceholder", "Placeholder", FORCE_STRING)
AccessorFunc(PANEL, "m_cplaceholderColor", "PlaceholderColor")
AccessorFunc(PANEL, "m_bTranslatePlaceholder", "TranslatePlaceholder", FORCE_BOOL)
AccessorFunc(PANEL, "m_iconColor", "IconColor")
-- Backwards
AccessorFunc(PANEL, "m_iCornerRadius", "RoundedCorners", FORCE_NUMBER)

function PANEL:Init()
    --self.Theme = DarkUI.GetTheme()
    self.Margin = Scale(6)
    self.IMargin = Scale(8)
    self:SetCornerRadius(self.Margin)
    self:SetPlaceholder("")
    -- These will overwrite the Theme settings
    --self:SetBackgroundColor(Color(x, x, x))
    --self:SetTextColor(Color(x, x, x))
    --self:SetPlaceholderColor(Color(x, x, x)) 
    self:SetTall(Scale(30))
    self.textentry = vgui.Create("DTextEntry", self)
    self.textentry:SetDrawLanguageID(false)
    self.textentry:SetFont("DarkUI.Text")
    self.textentry:DockMargin(self.Margin, self.Margin, self.Margin, self.Margin)
    self.textentry:Dock(FILL)

    self.textentry.Paint = function(pnl, w, h)
        local col = self.m_ctextColor or self.Theme.Text.Default
        pnl:DrawTextEntryText(col, self.Theme.Accent, col)

        if (#pnl:GetText() == 0) then
            local placholder = self.m_splaceholder or ""
            if placeholder == "" then return end

            if self.m_bTranslatePlaceholder then
                if string_StartWith(placholder, "#") then
                    placholder = string_sub(placholder, 2)
                end

                placholder = GetPhrase(placholder)
            end

            draw_SimpleText(placholder, pnl:GetFont(), 3, pnl:IsMultiline() and Scale(9.5) or h / 2, self.m_cplaceholderColor or self.Theme.Text.Darker, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
        end
    end

    self:MirrorChild("SetNumeric", self.textentry)
    self:MirrorChild("GetNumeric", self.textentry)
    self:MirrorChild("SetFont", self.textentry)
    self:MirrorChild("GetFont", self.textentry)
    self:MirrorChild("SetText", self.textentry)
    self:MirrorChild("GetText", self.textentry)
    self:MirrorChild("GetInt", self.textentry)
    self:MirrorChild("GetFloat", self.textentry)
    self:MirrorChild("SetValue", self.textentry)
    self:MirrorChild("GetValue", self.textentry)
    self:MirrorChild("SetEnterAllowed", self.textentry)
    self:MirrorChild("GetEnterAllowed", self.textentry)
    self:MirrorChild("IsEditing", self.textentry)
    self:MirrorChild("SetUpdateOnType", self.textentry)
    self:MirrorChild("GetUpdateOnType", self.textentry)
    self:MirrorChild("RequestFocus", self.textentry)
    --
    self:OverwriteChildFunc("OnEnter", self.textentry)
    self:OverwriteChildFunc("OnChange", self.textentry)
    self:OverwriteChildFunc("AutoComplete", self.textentry)
    self:OverwriteChildFunc("OnKeyCodeTyped", self.textentry)
    self:OverwriteChildFunc("AllowInput", self.textentry)
    self:OverwriteChildFunc("OnTextChanged", self.textentry)
    self:OverwriteChildFunc("OnValueChange", self.textentry)
end

-- TextEntry, in case i need more access
function PANEL:GetTextEntry()
    return self.textentry
end

-- Icon
function PANEL:SetupIcon(left)
    if not IsValid(self.icon) then
        self.IMargin = self.IMargin or 0
        self.icon = vgui.Create("DButton", self)
        self.icon:Dock(left and LEFT or RIGHT)
        self.icon:DockMargin(left and self.IMargin or (self.IMargin * -1), self.IMargin, left and 0 or self.IMargin, self.IMargin)
        self.icon:SetText("")

        self.icon.Paint = function(pnl, w, h)
            if not self.Theme then return end

            if self.icon.Loading then
                DarkLib.DrawLoading(w / 2, h / 2, h, self.m_iconColor or self.Theme.Accent)
            elseif pnl.mat and not pnl.mat:IsError() then
                surface.SetDrawColor(self.m_iconColor or self.Theme.Accent)
                surface.SetMaterial(pnl.mat)
                surface.DrawTexturedRect(0, 0, w, h)
            end
        end

        self.icon.DoClick = function(pnl)
            self.textentry:RequestFocus()
        end
    end

    return self.icon
end

function PANEL:SetIcon(icon, left)
    local icon = self:SetupIcon(left)
    icon.mat = icon
end

function PANEL:SetImgurIcon(image_id, left)
    local icon = self:SetupIcon(left)
    icon.Loading = true

    -- Load Image
    DarkLib.Images.QueueLoadMaterials({
        image = image_id,
        func = function(mat)
            icon.Loading = false
            icon.mat = mat
        end
    })
end

function PANEL:GetIcon()
    return self.icon or self:SetupIcon()
end

-- Settings
function PANEL:SetMultiLine(state, scrollbar)
    self:SetMultiline(state)
    self.textentry:SetMultiline(state)
    self.textentry:SetVerticalScrollbarEnabled(scrollbar or false)

    return self
end

-- Required stuff
function PANEL:OnMousePressed()
    self.textentry:RequestFocus()
end

function PANEL:Paint(w, h)
    draw.RoundedBox(self.m_iCornerRadius or 0, 0, 0, w, h, self.m_cbackgroundColor or self.Theme.Primary)
end

function PANEL:PerformLayout(w, h)
    if (IsValid(self.icon)) then
        self.icon:SetWide(self.icon:GetTall())
    end
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local ctrl = vgui.Create(ClassName)
    --ctrl:SetTheme("BmanGames")
    --ctrl:SetNumeric(true)
    ctrl:SetText("Edit Me!")
    ctrl:SetPlaceholder("Placeholder!")
    --ctrl:SetMultiLine(true, true)
    ctrl:SetWide(200)
    ctrl:SetIcon(DarkLib.Materials.Cross)
    ctrl:SetImgurIcon("HgMhjrI")

    ctrl:OnEnter(function(self)
        Derma_Message("You Typed: " .. self:GetValue())
    end)

    PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
end

derma.DefineControl("DarkUI.Input", "A Simple Input", PANEL, "DarkUI_Panel")