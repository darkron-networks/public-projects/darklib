--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0

    May be done later
]]
local PANEL = {}

function PANEL:Init()
    self:SetFont("DarkUI.Text")
    self:SetColor(DarkUI.Theme.Text.Default)
end

derma.DefineControl("DarkUI.Label", "A simple Label", PANEL, "DLabel")
derma.DefineControl("DarkUI_Label", "Deprecated", PANEL, "DLabel")