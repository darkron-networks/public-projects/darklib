local PANEL = {}
local Scale = DarkLib.Scale
local S6, S8, S36 = Scale(6), Scale(8), Scale(36)
AccessorFunc(PANEL, "m_pParentPanel", "ParentPanel")
AccessorFunc(PANEL, "m_iCornerRadius", "CornerRadius", FORCE_NUMBER)

function PANEL:Init()
    self.Theme = DarkUI.GetTheme()
    -- TODO
end

function PANEL:Paint(w, h)
    -- TODO
end

function PANEL:PerformLayout(w, h)
    -- TODO
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local ctrl = vgui.Create(ClassName)
    ctrl:SetWide(200)
    PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
end

vgui.Register("DarkUI.Dropdown", PANEL, "EditablePanel")