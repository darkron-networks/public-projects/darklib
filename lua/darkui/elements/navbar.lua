--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local _C, ScreenScale = DarkLib.ModifyColor, DarkLib.Scale
--[[
    Navbar Button
]]
local PANEL = {}

function PANEL:Init()
    self.Theme = DarkUI.GetTheme()
    self:SetTextColor(self.Theme.Text.Default)
end

function PANEL:Paint(w, h)
    if self.m_bActive then
        local lineHeight = ScreenScale(2)
        surface.SetDrawColor(self.Theme.Accent)
        surface.DrawRect(0, h - lineHeight, w, lineHeight)
    end
end

function PANEL:SetActive(bool)
    self.m_bActive = bool

    if self.m_bActive then
        self:SetTextColor(self.Theme.Accent)
    else
        self:SetTextColor(self.Theme.Text.Default)
    end
end

vgui.Register("DarkUI_NavbarBtn", PANEL, "DarkUI_Button")
--[[
    Navbar
]]
local PANEL = {}
AccessorFunc(PANEL, "m_body", "Body")
AccessorFunc(PANEL, "m_bBig", "Big", FORCE_BOOL)
AccessorFunc(PANEL, "m_fFadeTime", "FadeTime")

function PANEL:Init()
    self.Theme = DarkUI.GetTheme()
    local Padding = ScreenScale(2)
    self.m_fFadeTime = .1
    self.buttons = {}
    self.panels = {}
    self.tabScroller = vgui.Create("DarkUI_HorizontalScroller", self) -- TODO: own implementation?
    self.tabScroller:Dock(FILL)
    self:DockPadding(0, Padding, 0, Padding)
    --self.animFade = Derma_Anim("Fade", self, self.CrossFade)
end

function PANEL:AddTab(name, pnl)
    -- Button
    local i = #self.buttons + 1
    self.buttons[i] = self:Add("DarkUI_NavbarBtn")
    local btn = self.buttons[i]
    btn.id = i
    btn:Dock(LEFT)
    btn:SetText(name)

    btn.DoClick = function(pnl)
        self:SetActive(pnl.id)
    end

    self.tabScroller:AddPanel(btn) -- TODO: own implementation?
    -- Panel
    self.panels[i] = self:GetBody():Add(pnl or "DPanel")
    local panel = self.panels[i]
    panel:Dock(FILL)
    panel:SetVisible(false)

    return panel
end

function PANEL:OnActiveChange(old, new)
end

function PANEL:ScrollToChild(panel)
    self.tabScroller:ScrollToChild(panel)
end

function PANEL:SetActive(id)
    if self.active == id then return end
    local btn = self.buttons[id]
    local panel = self.panels[id]
    if not IsValid(btn) then return end
    if not IsValid(panel) then return end
    -- Button
    local activeBtn = self.buttons[self.active]

    if IsValid(activeBtn) then
        activeBtn:SetActive(false)
    end

    -- Panel
    --self.animFade:Start(self:GetFadeTime(), {
    --    OldTab = self.panels[self.active],
    --    NewTab = panel
    --})
    local activePnl = self.panels[self.active]

    if IsValid(activePnl) then
        activePnl:SetVisible(false)
    end

    self.active = id
    btn:SetActive(true)
    panel:SetVisible(true)
    self:OnActiveChange(activeBtn and activeBtn.id or false, id)
end

function PANEL:SetActiveName(name, force)
    for i = 1, #self.buttons do
        local btn = self.buttons[i]
        if not IsValid(btn) then continue end

        if force then
            if string.StartWith(btn:GetText(), name) then
                self:SetActive(btn.id)
                self:ScrollToChild(btn)
            end
        else
            if btn:GetText() == name then
                self:SetActive(btn.id)
                self:ScrollToChild(btn)
            end
        end
    end
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(self.Theme.Background)
    surface.DrawRect(0, 0, w, h)
end

function PANEL:PerformLayout(w, h)
    local ButtonSpace = 1
    local Height = 1

    if self.m_bBig then
        ButtonSpace = ScreenScale(16)
        Height = ScreenScale(32)
    else
        ButtonSpace = ScreenScale(12)
        Height = ScreenScale(24)
    end

    self:SetTall(Height)

    for i = 1, #self.buttons do
        local btn = self.buttons[i]
        if not IsValid(btn) then continue end
        btn:SizeToContentsX(ButtonSpace)
    end
end

function PANEL:Think()
    --self.animFade:Run()
end

function PANEL:CrossFade(anim, delta, data)
    if (not data or not IsValid(data.OldTab) or not IsValid(data.NewTab)) then return end
    local old = data.OldTab
    local new = data.NewTab
    if (not IsValid(old) and not IsValid(new)) then return end

    if (anim.Finished) then
        if (IsValid(old)) then
            old:SetAlpha(255)
            old:SetZPos(0)
            old:SetVisible(false)
        end

        if (IsValid(new)) then
            new:SetAlpha(255)
            new:SetZPos(0)
            new:SetVisible(true) -- In case new == old
        end

        return
    end

    if (anim.Started) then
        if (IsValid(old)) then
            old:SetAlpha(255)
            old:SetZPos(0)
        end

        if (IsValid(new)) then
            new:SetAlpha(0)
            new:SetZPos(1)
        end
    end

    if (IsValid(old)) then
        old:SetVisible(true)

        if (not IsValid(new)) then
            old:SetAlpha(255 * (1 - delta))
        end
    end

    if (IsValid(new)) then
        new:SetVisible(true)
        new:SetAlpha(255 * delta)
    end
end

derma.DefineControl("DarkUI_Navbar", "Navbar", PANEL)