local Scale, ModColor = DarkLib.Scale, DarkLib.ModifyColor
local PANEL = {}
AccessorFunc(PANEL, "m_bBorder", "DrawBorder", FORCE_BOOL)
AccessorFunc(PANEL, "m_iCornerRadius", "CornerRadius", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bBorderSize", "BorderSize", FORCE_NUMBER)

function PANEL:Init()
    self.Theme = DarkUI.GetTheme()
    self.ThemeOverwrite = {}
    self:SetContentAlignment(5)
    -- 
    self:SetTall(Scale(34))
    self:SetDrawBorder(true)
    self:SetPaintBackground(true)
    self:SetMouseInputEnabled(true)
    self:SetKeyboardInputEnabled(true)
    self:SetCursor("hand")
    self:SetFont("DarkUI.Text")
    self:SetCornerRadius(Scale(6))
    self:SetBorderSize(0)
end

--[[
    Theme stuff
]]
function PANEL:SetTheme(name)
    self.Theme = DarkUI.GetTheme(name)
end

--[[
    DoClick Function Handler
    if function is given, set DoClickFunc to func
    otherwise run the function if already set.
]]
function PANEL:DoClick(func)
    if not self:Validated() or self.Loading then return end

    -- Set Custom handler
    if func then
        self.DoClickFunc = isfunction(func) and func or false

        return
    end

    -- Run Custom handler
    if self.DoClickFunc then return self.DoClickFunc(self) end
end

--[[
    Show (if true) the Loading circle for a givem amount of time and block input.
]]
function PANEL:SetLoading(bool, time)
    self.Loading = bool
    self.LoadingTimeout = (time and CurTime() + time) or false

    if self.Loading then
        if self.m_oldtext ~= nil then return end
        self.m_oldtext = self:GetText()
        self:SetText("")
    else
        if self.m_oldtext == nil then return end
        self:SetText(self.m_oldtext or "")
        self.m_oldtext = nil
    end

    self:UpdateCursor()
end

function PANEL:SizeToContents()
    local w, h = self:GetContentSize()
    self:SetSize(w + Scale(10), h + Scale(6))
end

-- Events
function PANEL:Validated()
    --return true to allow input (click)
    return true
end

function PANEL:UpdateColours(skin)
    if (not self:IsEnabled()) then return self:SetTextStyleColor(self.Theme.Text.Darker) end
    if (self:IsDown() or self.m_bSelected) then return self:SetTextStyleColor(self.Theme.Text.Darker) end
    --if (self.Hovered) then return self:SetTextStyleColor(skin.Colours.Button.Hover) end

    return self:SetTextStyleColor(self.Theme.Text.Default)
end

function PANEL:UpdateCursor()
    if self:GetDisabled() or self.Loading or not self:Validated() then
        self:SetCursor("no")
    else
        self:SetCursor("hand")
    end
end

function PANEL:OnCursorEntered()
    self:UpdateCursor()
    self.BaseClass.OnCursorEntered(self)
end

function PANEL:Think()
    if self.LoadingTimeout and self.LoadingTimeout < CurTime() then
        self:SetLoading(false, false)
    end
end

function PANEL:Paint(w, h)
    -- Set vars
    local Background = self.ThemeOverwrite.Background or self.Theme.Accent
    local DisabledBackground = self.ThemeOverwrite.DisabledBackground or ModColor(self.Theme.Accent, 0.7)
    local DisabledInvalid = self.ThemeOverwrite.InvalidBackground or self.Theme.Red
    -- Draw stuff
    draw.RoundedBox((self.m_iCornerRadius or 0) + (self.m_bBorderSize > 0 and 1 or 0), 0, 0, w, h, (not self:Validated() and DisabledInvalid) or (self:GetDisabled() and DisabledBackground) or Background)

    if self.m_bBorderSize > 0 then
        DarkLib.DrawOutlinedBoxRounded(self.m_iCornerRadius, 0, 0, w, h, self.m_bBorderSize, self.ThemeOverwrite.Border or self.Theme.Primary)
    end

    if self.Loading then
        DarkLib.DrawLoading(w / 2, h / 2, h - (h * 0.3), self.ThemeOverwrite.Loader or ModColor(self.Theme.Accent, 1.4))
    end
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local ctrl = vgui.Create(ClassName)
    ctrl:SetText("Example Button")
    ctrl:SizeToContents()
    ctrl:SetDisabled(false)

    ctrl:DoClick(function()
        print("hi")
    end)

    PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
end

derma.DefineControl("DarkUI.Button", "A Simple Button", PANEL, "DButton")