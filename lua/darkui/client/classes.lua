--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
--[[
    Colors
]]
DarkUI.TextColors = {
    Normal = Color(255, 255, 255),
    MoreAlpha = Color(255, 255, 255, 166),
    Alpha = Color(255, 255, 255, 180),
    LessAlpha = Color(255, 255, 255, 200),
    Disabled = Color(200, 200, 200),
    Invalid = Color(200, 200, 200)
}

DarkUI.Flat_Colors = {
    Dark = Color(33, 33, 33),
    Gray = Color(102, 102, 102),
    LightGray = Color(119, 119, 119),
    DarkGray = Color(85, 85, 85),
    Text = DarkUI.TextColors.Normal,
    TextAlpha = DarkUI.TextColors.MoreAlpha,
    Blue = Color(21, 101, 192),
    Green = Color(67, 160, 71),
    Red = Color(213, 0, 0),
    LightRed = Color(255, 82, 82),
    DarkRed = Color(183, 28, 28)
}

local Flat_Colors = DarkUI.Flat_Colors

--[[
    DButton = {
        -- Background Colors
        Default = Color(3, 83, 164),
        Hover = Color(3 * 1.3, 83 * 1.3, 164 * 1.3),
        Pressed = Color(3, 83, 164),
        Disabled = Color(51, 65, 92),
        Invalid = Color(191, 35, 19),
        -- Text Colors
        DefaultText = Flat_Colors.TextAlpha,
        HoverText = Color(255, 255, 255, 180),
        PressedText = Color(255, 255, 255, 200),
        DisabledText = Color(200, 200, 200),
        InvalidText = Color(200, 200, 200)
    },
]]
DarkUI.Colors = {
    DButton = {
        -- Background Colors
        Default = Color(92 * .9, 103 * .9, 125 * .9),
        Hover = Color(92 * .8, 103 * .8, 125 * .8),
        Pressed = Color(92 * .7, 103 * .7, 125 * .7),
        Disabled = Color(51, 65, 92),
        Invalid = Color(191, 35, 19),
        -- Text Colors
        DefaultText = Flat_Colors.TextAlpha,
        HoverText = Color(255, 255, 255, 180),
        PressedText = Color(255, 255, 255, 200),
        DisabledText = Color(200, 200, 200),
        InvalidText = Color(200, 200, 200)
    },
    DTextEntry = {
        background = {
            enabled = Color(102, 102, 102, 50),
            disabled = Color(102 * 0.8, 102 * 0.8, 102 * 0.8, 50)
        },
        cursorColor = Color(255, 255, 255, 200),
        textColor = Flat_Colors.TextAlpha,
        highlightColor = Color(42, 202, 255, 100),
        placeholderTextColor = Color(255, 255, 255, 90)
    },
    --
    DVScrollBar = {
        Bar = {
            default = Color(220, 220, 220, 90),
            Hover = Color(220, 220, 220, 140)
        },
        Buttons = {
            default = Color(220, 220, 220, 100),
            Hover = Color(220, 220, 220, 150)
        }
    },
    DListView = {
        background = Color(33, 33, 33, 180),
        Header = {
            default = Color(0, 0, 0),
            Hover = Color(10, 10, 10)
        },
        Item = {
            default = Color(0, 0, 0),
            Hover = Color(10, 10, 10)
        }
    },
    DListView_Line = {
        background = nil,
        backgroundAlt = Color(40, 40, 40),
        Hover = Flat_Colors.DarkGray,
        Selected = Flat_Colors.Gray
    },
    DFrame = {
        backgroundAlpha = Color(33, 33, 33, 220),
        background = Color(33, 33, 33),
        headerAlpha = Color(47, 47, 47, 240),
        header = Color(47, 47, 47)
    },
    frame = {
        friendList = {
            background = Color(200, 200, 200, 10)
        },
        ChatPanel = {
            background = Color(100, 79, 79, 200)
        },
        InfoPanel = {
            background = Color(100, 79, 79, 200)
        }
    }
}

--[[
    Utils
]]
local DrawTriangle = DarkLib.DrawTriangle

--[[---------------------------------------------------------
	DFrame
    Moved to darkui_frame
-----------------------------------------------------------]]
DarkUI.RegisterClass("DFrame", {
    Paint = function(self, w, h)
        local backgroundColor = self._Transparent and DarkUI.Colors.DFrame.backgroundAlpha or DarkUI.Colors.DFrame.background
        local headerColor = self._Transparent and DarkUI.Colors.DFrame.headerAlpha or DarkUI.Colors.DFrame.header
        draw.RoundedBox(self.m_bCornersRadius or 4, 0, 0, w, h, backgroundColor)
        draw.RoundedBoxEx(self.m_bCornersRadius or 4, 0, 0, w, 24, headerColor, self.m_bCornersRadius or 4, self.m_bCornersRadius or 4)

        if self._backgroundMaterial then
            local mat = self._backgroundMaterial
            local mode = self._backgroundMaterialMode or "stretch"
            surface.SetDrawColor(255, 255, 255, 100)
            surface.SetMaterial(mat)

            if mode == "stretch" then
                surface.DrawTexturedRect(0, 20, w, h)
            elseif mode == "center" then
                surface.DrawTexturedRect((w / 2) - (h / 2), 20, h, h - 30)
            end
        end

        if (self.m_bBackgroundBlur) then
            Derma_DrawBackgroundBlur(self, self.m_fCreateTime)
        end

        return false
    end,
    SetBackgroundMaterial = function(self, material, mode)
        if isstring(material) and type(material) ~= "IMaterial" then
            material = Material(material, "noclamp smooth")
        end

        self._backgroundMaterial = material
        self._backgroundMaterialMode = mode
    end,
    SetParentFrame = function(self, frame)
        if IsValid(frame) and frame:GetName() == "DFrame" and self:GetName() == "DFrame" then
            frame._toRemoveOnRemove = frame._toRemoveOnRemove or {}
            table.insert(frame._toRemoveOnRemove, self)
        end
    end,
    SetTransparent = function(self, enable)
        self._Transparent = enable
    end,
    SetMinimizeAction = function(self, callback)
        if isfunction(callback) then
            self.btnMinim:Show()
            self.btnMinim.DoClick = callback
            self.btnMinim:SetDisabled(false)
        else
            self.btnMinim:Hide()
            self.btnMinim:SetDisabled(true)
        end

        self:InvalidateLayout(true)
    end,
    SetMaximizeAction = function(self, callback)
        if isfunction(callback) then
            self.btnMaxim:Show()
            self.btnMaxim.DoClick = callback
            self.btnMaxim:SetDisabled(false)
        else
            self.btnMaxim:Hide()
            self.btnMaxim:SetDisabled(true)
        end

        self:InvalidateLayout(true)
    end,
    SetupClass = function(self)
        AccessorFunc(self, "m_bCornersRadius", "CornerRadius", FORCE_NUMBER)
        self:SetMinHeight(80)
        self:SetMinWidth(150)
        --[[
            Remove child frames on removal
        ]]
        local oldRemove = self.OnRemove or function() end

        self.OnRemove = function(self, ...)
            for i, frame in ipairs(self._toRemoveOnRemove or {}) do
                if IsValid(frame) then
                    frame:Remove()
                end
            end

            return oldRemove and oldRemove(self, ...) or nil
        end

        --[[
            Button Replacement
        ]]
        if IsValid(self.btnClose) then
            self.btnClose:Remove()
        end

        if IsValid(self.btnMaxim) then
            self.btnMaxim:Remove()
        end

        if IsValid(self.btnMinim) then
            self.btnMinim:Remove()
        end

        -- Button Panel
        self.btnPanel = DarkUI.Create("DPanel", self)
        -- Close button
        self.btnClose = DarkUI.Create("DButton", self.btnPanel) --vgui.Create("FButton", self)
        self.btnClose:Dock(RIGHT)
        self.btnClose.Paint = function() end
        self.btnClose:SetText("X")
        self.btnClose:SetFont("DarkUI.FrameButton")

        self.btnClose.DoClick = function(button)
            self:Close()
        end

        self.btnMaxim = DarkUI.Create("DButton", self.btnPanel) --vgui.Create("FButton", self)
        self.btnMaxim:Dock(RIGHT)
        self.btnMaxim.Paint = function() end
        self.btnMaxim:SetText("☐")
        self.btnMaxim:SetFont("DarkUI.FrameButton")

        self.btnMaxim.DoClick = function(button)
            self:Close()
        end

        self.btnMinim = DarkUI.Create("DButton", self.btnPanel) --vgui.Create("FButton", self)
        self.btnMinim:Dock(RIGHT)
        self.btnMinim.Paint = function() end
        self.btnMinim:SetText("_")
        self.btnMinim:SetFont("DarkUI.FrameButton")

        self.btnMinim.DoClick = function(button)
            self:Close()
        end

        self.btnMaxim:SetDisabled(true)
        self.btnMinim:SetDisabled(true)
        self.btnMaxim:Hide(true)
        self.btnMinim:Hide(true)
        --[[
            Perform Layout for minimal sizes
        ]]
        local performlayout = self.PerformLayout

        self.PerformLayout = function(self, w, h)
            if isnumber(w) and w < self:GetMinWidth() then
                self:SetWide(self:GetMinWidth())
            end

            if isnumber(h) and h < self:GetMinHeight() then
                self:SetTall(self:GetMinHeight())
            end

            performlayout(self, w, h)
            -- Butten Panel setup
            local wide = 0

            if self.btnClose:IsVisible() then
                wide = wide + 22
            end

            if self.btnMinim:IsVisible() then
                wide = wide + 22
            end

            if self.btnMaxim:IsVisible() then
                wide = wide + 22
            end

            if wide > 0 then
                if wide == 22 then
                    wide = wide + 4
                end

                if wide == 44 then
                    wide = wide + 12
                end

                if wide == 66 then
                    wide = wide + 22
                end

                self.btnPanel:Show()
                self.btnPanel:SetPos(self:GetWide() - wide, 0)
                self.btnPanel:SetSize(wide, 25)
            else
                self.btnPanel:Hide()
            end
        end

        -- For older stuff
        self.SetBakgroundMaterial = function(...)
            if DarkLib.DEBUG then
                DarkLib:LogDebug("Using Deprecated SetBakgroundMaterial!")
                debug.Trace()
            end

            self.SetBackgroundMaterial(...)
        end
    end
})

--[[---------------------------------------------------------
	DPanel
    Moved to darkui_panel
-----------------------------------------------------------]]
DarkUI.RegisterClass("DPanel", {
    Paint = function(self, w, h) return false end,
    GetContentSize = function(self) return self:ChildrenSize() end,
    Think = function(self)
        if (self:GetAutoStretchVertical()) then
            self:SizeToContentsY()
        end
    end,
    SetupClass = function(self)
        AccessorFunc(self, "m_bAutoStretchVertical", "AutoStretchVertical", FORCE_BOOL)
    end
})

--[[---------------------------------------------------------
	DButton
    (DButton, color, colorHovered, ColorInvalid)

    Example Skin:
    
    CallButton:CreateTheme("default", {
        default = Color(180, 220, 180, 160),
        defaultText = Color(40, 40, 40),
        hover = Color(180, 220, 180, 200),
        hoverText = nil,
        disabled = nil,
        disabledText = nil,
        invalid = nil,
        invalidText = nil,
        pressed = nil,
        pressedText = nil
    })
    Moved to DarkUI.Text
-----------------------------------------------------------]]
DarkUI.RegisterClass("DButton", {
    Paint = function(self, w, h)
        local DisabledColor = self._disabledColor or DarkUI.Colors.DButton.Disabled

        if self:GetDisabled() then
            draw.RoundedBox(self.m_bCornersRadius or 0, 0, 0, w, h, DisabledColor)
        else
            local InvalidColor = self._invalidColor or DarkUI.Colors.DButton.Invalid
            local DefaultColor = self._defaultColor or DarkUI.Colors.DButton.Default
            local HoverColor = self._hoverColor or DarkUI.Colors.DButton.Hover
            local PressedColor = self._pressedColor or DarkUI.Colors.DButton.Pressed
            --draw.RoundedBox(self.m_bCornersRadius, 0, 0, w, h, self:checkValid() and ((self:IsHovered() and not self.Depressed) and HoverColor or DefaultColor) or InvalidColor)
            draw.RoundedBox(self.m_bCornersRadius or 0, 0, 0, w, h, (not self:checkValid() and InvalidColor) or (self.Depressed and PressedColor) or (self:IsHovered() and HoverColor) or DefaultColor)
        end

        return false
    end,
    UpdateColours = function(self, skin)
        if (not self:IsEnabled()) then return self:SetTextStyleColor(self._disabledTextColor or DarkUI.Colors.DButton.DisabledText) end
        if (not self:checkValid()) then return self:SetTextStyleColor(self._invalidTextColor or DarkUI.Colors.DButton.InvalidText) end
        if (self:IsDown() or self.m_bSelected) then return self:SetTextStyleColor(self._pressedTextColor or DarkUI.Colors.DButton.PressedText) end
        if (self.Hovered) then return self:SetTextStyleColor(self._hoverTextColor or DarkUI.Colors.DButton.HoverText) end

        return self:SetTextStyleColor(self._defaultTextColor or DarkUI.Colors.DButton.DefaultText)
    end,
    checkValid = function(self) return true end, -- to overwrite
    SetTheme = function(self, theme)
        self._skins = self._skins or {}
        local skin = self._skins[theme or "default"]
        if not skin then return end
        self:SetDefaultColor(skin.default, skin.defaultText)
        self:SetHoverColor(skin.hover, skin.hoverText)
        self:SetDisabledColor(skin.disabled, skin.disabledText)
        self:SetInvalidColor(skin.invalid, skin.invalidText)
        self:SetPressedColor(skin.pressed, skin.pressedText)
        self:UpdateColours()

        return self
    end,
    CreateTheme = function(self, name, skin)
        self._skins = self._skins or {}
        self._skins[name] = skin

        if name == "default" then
            self:SetTheme("default")
        end
    end,
    -- Default Colors
    _defaultColor = nil,
    _defaultTextColor = nil,
    -- Colors when pressed
    _pressedColor = nil,
    _pressedTextColor = nil,
    -- Colors when hovered
    _hoverColor = nil,
    _hoverTextColor = nil,
    -- Colors when invalid
    _invalidColor = nil,
    _invalidTextColor = nil,
    -- Colors when disabled
    _disabledColor = nil,
    _disabledTextColor = nil,
    -- Setters
    SetDefaultColor = function(self, color, textColor)
        self._defaultColor = IsColor(color) and color or DarkUI.Colors.DButton.Default
        self._defaultTextColor = IsColor(textColor) and textColor or DarkUI.Colors.DButton.DefaultText
    end,
    SetHoverColor = function(self, color, textColor)
        self._hoverColor = IsColor(color) and color or DarkUI.Colors.DButton.Hover
        self._hoverTextColor = IsColor(textColor) and textColor or DarkUI.Colors.DButton.HoverText
    end,
    SetDisabledColor = function(self, color, textColor)
        self._disabledColor = IsColor(color) and color or DarkUI.Colors.DButton.Disabled
        self._disabledTextColor = IsColor(textColor) and textColor or DarkUI.Colors.DButton.DisabledText
    end,
    SetInvalidColor = function(self, color, textColor)
        self._invalidColor = IsColor(color) and color or DarkUI.Colors.DButton.Invalid
        self._invalidTextColor = IsColor(textColor) and textColor or DarkUI.Colors.DButton.InvalidText
    end,
    SetPressedColor = function(self, color, textColor)
        self._pressedColor = IsColor(color) and color or DarkUI.Colors.DButton.Pressed
        self._pressedTextColor = IsColor(textColor) and textColor or DarkUI.Colors.DButton.PressedText
    end,
    SetupClass = function(self)
        AccessorFunc(self, "m_bCornersRadius", "CornerRadius", FORCE_NUMBER)
    end
})

--[[---------------------------------------------------------
	DTextEntry
    Moved to darkui_textentry
-----------------------------------------------------------]]
DarkUI.RegisterClass("DTextEntry", {
    Paint = function(self, w, h)
        if not self.NoBackground then
            if self:GetDisabled() then
                draw.RoundedBox(0, 0, 0, w, h, self._mBackgroundDisabled or DarkUI.Colors.DTextEntry.background.disabled)
            else
                draw.RoundedBox(0, 0, 0, w, h, self._mBackgroundEnabled or DarkUI.Colors.DTextEntry.background.enabled)
            end
        end

        if (self.GetPlaceholderText and self.GetPlaceholderColor and self:GetPlaceholderText() and self:GetPlaceholderText():Trim() ~= "" and self:GetPlaceholderColor() and (not self:GetText() or self:GetText() == "")) then
            local oldText = self:GetText()
            local str = self:GetPlaceholderText()

            if (str:StartWith("#")) then
                str = str:sub(2)
            end

            str = language.GetPhrase(str)
            self:SetText(str)
            self:DrawTextEntryText(self:GetPlaceholderColor(), self:GetHighlightColor(), self:GetCursorColor())
            self:SetText(oldText)

            return
        end

        --draw.RoundedBox(0, 0, h - 2, w, 2, self._mBackgroundDisabled or DarkUI.Colors.DTextEntry.background.disabled)
        self:DrawTextEntryText(self:GetTextColor(), self:GetHighlightColor(), self:GetCursorColor())
    end,
    SetPlaceholder = function(self, text)
        self.m_txtPlaceholder = text or nil
    end,
    -- Color Setters
    SetBackgroundColor = function(self, enabled, disabled)
        self._mBackgroundEnabled = IsColor(enabled) and enabled or nil
        self._mBackgroundDisabled = IsColor(disabled) and disabled or nil
    end,
    SetTextColor = function(self, color)
        self._mColText = IsColor(color) and color or nil
    end,
    SetPlaceholderColor = function(self, color)
        self.m_colPlaceholder = IsColor(color) and color or nil
    end,
    SetHighlightColor = function(self, color)
        self.m_colHighlight = IsColor(color) and color or nil
    end,
    SetCursorColor = function(self, color)
        self.m_colCursor = IsColor(color) and color or nil
    end,
    -- Color Getters
    GetTextColor = function(self) return self.m_colText or DarkUI.Colors.DTextEntry.textColor end,
    GetPlaceholderColor = function(self) return self.m_colPlaceholder or DarkUI.Colors.DTextEntry.placeholderTextColor end,
    GetHighlightColor = function(self) return self.m_colHighlight or DarkUI.Colors.DTextEntry.highlightColor end,
    GetCursorColor = function(self) return self.m_colCursor or DarkUI.Colors.DTextEntry.cursorColor end
})

--[[---------------------------------------------------------
	DVScrollBar
-----------------------------------------------------------]]
DarkUI.RegisterClass("DVScrollBar", {
    SetupClass = function(self)
        self:SetWide(12)
        self:DockMargin(0, 0, 0, 0)

        self.IsDragged = function(self)
            -- if self.btnGrip:IsDown() then return true end
            if self.Dragging then return true end
            if self:IsHovered() then return true end
            if self.btnGrip:IsHovered() then return true end
            if self.btnUp:IsHovered() then return true end
            if self.btnDown:IsHovered() then return true end

            return false
        end

        self.Paint = function() end

        self.btnGrip.Paint = function(self, w, h)
            draw.RoundedBox(2, 2, 0, w - 4, h, self:IsHovered() and DarkUI.Colors.DVScrollBar.Bar.Hover or DarkUI.Colors.DVScrollBar.Bar.default)
        end

        self.btnGrip:SetCursor("hand")

        self.btnUp.Paint = function(self, w, h)
            if self:IsHovered() then
                surface.SetDrawColor(DarkUI.Colors.DVScrollBar.Buttons.Hover)
            else
                surface.SetDrawColor(DarkUI.Colors.DVScrollBar.Buttons.default)
            end

            draw.NoTexture()
            DrawTriangle(w - (w / 2), h - (h / 2), w - 4, h - 4)
        end

        self.btnDown.Paint = function(self, w, h)
            if self:IsHovered() then
                surface.SetDrawColor(DarkUI.Colors.DVScrollBar.Buttons.Hover)
            else
                surface.SetDrawColor(DarkUI.Colors.DVScrollBar.Buttons.default)
            end

            draw.NoTexture()
            DrawTriangle(w - (w / 2), h - (h / 2), w - 4, h - 4, 180)
        end

        self.btnUp.DoClick = function(self)
            self:GetParent():AddScroll(-2)
        end

        self.btnDown.DoClick = function(self)
            self:GetParent():AddScroll(2)
        end
    end
})

--[[---------------------------------------------------------
	DScrollPanel
-----------------------------------------------------------]]
DarkUI.RegisterClass("DScrollPanel", {
    SetupClass = function(self)
        DarkUI.Setup("DVScrollBar", self.VBar)
    end
})

--[[---------------------------------------------------------
	DNumSlider
-----------------------------------------------------------]]
DarkUI.RegisterClass("DNumSlider", {
    SetupClass = function(self)
        DarkUI.Setup("DTextEntry", self.TextArea)
        self.TextArea.NoBackground = true
    end
})

--[[---------------------------------------------------------
	DListView
-----------------------------------------------------------]]
DarkUI.RegisterClass("DListView_Column", {
    SetupClass = function(self)
        DarkUI.Setup("DButton", self.Header)
        self:ClearPaint()
    end
})

DarkUI.RegisterClass("DListView_ColumnPlain", {
    SetupClass = function(self)
        DarkUI.Setup("DButton", self.Header)
        self:ClearPaint()
    end
})

DarkUI.RegisterClass("DListViewLabel", {
    --    Paint = function() end,
    SetupClass = function(self) end
})

DarkUI.RegisterClass("DListView_Line", {
    Paint = function(self, w, h)
        if (self:IsSelected()) then
            draw.RoundedBox(0, 0, 0, w, h, DarkUI.Colors.DListView_Line.Selected)
        elseif (self.Hovered) then
            draw.RoundedBox(0, 0, 0, w, h, DarkUI.Colors.DListView_Line.Hover)
        elseif (self.m_bAlt) then
            draw.RoundedBox(0, 0, 0, w, h, DarkUI.Colors.DListView_Line.backgroundAlt)
        elseif DarkUI.Colors.DListView_Line.background then
            draw.RoundedBox(0, 0, 0, w, h, DarkUI.Colors.DListView_Line.background)
        end
    end,
    SetAlign = function(self, align)
        local aligns = {
            ["bottom-left"] = 1,
            ["bottom-center"] = 2,
            ["bottom-right"] = 3,
            ["middle-left"] = 4,
            ["center"] = 5,
            ["middle-right"] = 6,
            ["top-left"] = 7,
            ["top-center"] = 8,
            ["top-right"] = 9
        }

        self._Align = aligns[align or ""]
    end,
    SetupClass = function(self)
        local oldSetColumnText = self.SetColumnText

        self.SetColumnText = function(self, i, strText)
            local item = DarkUI.Setup("DListViewLabel", oldSetColumnText(self, i, strText))

            if item:GetName() == "DListViewLabel" then
                item:SetTextColor(Flat_Colors.TextAlpha)

                if self._Align then
                    item:SetContentAlignment(self._Align)
                end
            end

            return item
        end
    end
})

DarkUI.RegisterClass("DListView", {
    Paint = function(self, w, h)
        --surface.SetDrawColor(255, 255, 255, 255)
        --if not self.m_bHideHeaders then draw.RoundedBox(0, 0, 0, w, self.m_iHeaderHeight, DarkUI.Colors.DListView.background) end
        draw.RoundedBox(0, 0, 0, w, h, DarkUI.Colors.DListView.background)
    end,
    SetItemAlign = function(self, align)
        self._ItemAlign = align
    end,
    SetupClass = function(self)
        DarkUI.Setup("DVScrollBar", self.VBar)
        --DarkUI.Setup("DListViewLabel", self.VBar)
        local oldAddColumn = self.AddColumn -- (string column, string material, number position)

        self.AddColumn = function(self, column, material, position)
            local class = "DListView_ColumnPlain"

            if (self.m_bSortable) then
                class = "DListView_Column"
            end

            local col = oldAddColumn(self, column, material, position)

            return DarkUI.Setup(class, col)
        end

        self.AddLine = function(self, ...)
            self:SetDirty(true)
            self:InvalidateLayout()
            local Line = DarkUI.Create("DListView_Line", self.pnlCanvas)
            local ID = table.insert(self.Lines, Line)

            if self._ItemAlign and isstring(self._ItemAlign) then
                Line:SetAlign(self._ItemAlign)
            end

            Line:SetListView(self)
            Line:SetID(ID)

            -- This assures that there will be an entry for every column
            for k, v in pairs(self.Columns) do
                Line:SetColumnText(k, "")
            end

            for k, v in pairs({...}) do
                Line:SetColumnText(k, v)
            end

            -- Make appear at the bottom of the sorted list
            local SortID = table.insert(self.Sorted, Line)

            if (SortID % 2 == 1) then
                Line:SetAltLine(true)
            end

            return Line
        end
    end
})

-- This is only tamporary fow now (im still working on it)
local DTabColor = {
    text = Color(255, 255, 255),
    textDisabled = Color(190, 190, 190, 100)
}

DarkUI.RegisterClass("DTab", {
    Paint = function(self, w, h)
        --[[
        if not self:GetDisabled() then
            draw.RoundedBox(0, 0, 0, w, h, Flat_Colors.LightGray)
        else
            draw.RoundedBox(0, 0, 0, w, h, Flat_Colors.Gray)
        end
        --]]
        if (self:IsActive()) then
            draw.RoundedBox(0, 0, h - 2, w, 2, Flat_Colors.DarkGray)
        end
    end,
    GetTabHeight = function(self) return self:IsActive() and 22 or 20 end,
    UpdateColours = function(self, skin)
        --[[
        if (self:IsActive()) then
            if (self:GetDisabled()) then return self:SetTextStyleColor(skin.Colours.Tab.Active.Disabled) end
            --if (self:IsDown()) then return self:SetTextStyleColor(skin.Colours.Tab.Active.Down) end
            --if (self.Hovered) then return self:SetTextStyleColor(skin.Colours.Tab.Active.Hover) end

            return self:SetTextStyleColor(skin.Colours.Tab.Active.Normal)
        end
    --]]
        if (self:GetDisabled()) then return self:SetTextStyleColor(DTabColor.textDisabled) end
        --if (self:IsDown()) then return self:SetTextStyleColor(skin.Colours.Tab.Inactive.Down) end
        if (self.Hovered) then return self:SetTextStyleColor(skin.Colours.Tab.Inactive.Hover) end

        return self:SetTextStyleColor(DTabColor.text) --[[skin.Colours.Tab.Inactive.Normal]]
    end,
    -- TODO: using correct colors
    SetupClass = function(self)
        self:InvalidateLayout()
    end
})

DarkUI.RegisterClass("DPropertySheet", {
    AddSheet = function(self, label, panel, material, NoStretchX, NoStretchY, Tooltip)
        if (not IsValid(panel)) then
            ErrorNoHalt("DPropertySheet:AddSheet tried to add invalid panel!")
            debug.Trace()

            return
        end

        local Sheet = {}
        Sheet.Name = label
        Sheet.Tab = DarkUI.Create("DTab", self)
        Sheet.Tab:SetTooltip(Tooltip)
        Sheet.Tab:Setup(label, self, panel, material)
        Sheet.Panel = panel
        Sheet.Panel.NoStretchX = NoStretchX
        Sheet.Panel.NoStretchY = NoStretchY
        Sheet.Panel:SetPos(self:GetPadding(), 20 + self:GetPadding())
        Sheet.Panel:SetVisible(false)
        panel:SetParent(self)
        table.insert(self.Items, Sheet)

        if (not self:GetActiveTab()) then
            self:SetActiveTab(Sheet.Tab)
            Sheet.Panel:SetVisible(true)
        end

        self.tabScroller:AddPanel(Sheet.Tab)

        return Sheet
    end,
    SetupClass = function(self)
        DarkUI.Setup(self.tabScroller:GetName(), self.tabScroller, self)
        self:ClearPaint()
    end
})