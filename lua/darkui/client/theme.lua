--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
DarkUI.Themes = {}
DarkUI.CurrentTheme = "Default"
local rgb, _C = Color, DarkLib.ModifyColor
local TextColor = color_white -- One color to rule them all

DarkUI.Theme = {
    Primary = rgb(36, 38, 38),
    Background = rgb(54, 54, 58),
    Accent = rgb(92, 103, 125),
    Red = rgb(230, 58, 64),
    Green = rgb(46, 204, 113),
    Blue = rgb(41, 128, 185),
    Text = {
        Default = TextColor,
        Brighter = _C(TextColor, 1.3),
        Bright = _C(TextColor, 1.6),
        Darker = _C(TextColor, 0.6),
        Dark = _C(TextColor, 0.3)
    }
}

function DarkUI.CreateTheme(name, tbl, base)
    local Default = (base and DarkUI.Themes[name]) or DarkUI.Theme -- Can be based on other stuff
    DarkUI.Themes[name] = DarkLib.RecursiveTableMerge(table.Copy(Default), tbl)
    DARKUI:LogDebug("Created Theme " .. name)
    --PrintTable(DarkUI.Themes[name])
end

function DarkUI.SetTheme(newtheme)
    if not DarkUI.Themes[newtheme] then return end
    if DarkUI.CurrentTheme == newtheme then return end
    local oldTheme = DarkUI.CurrentTheme
    DarkUI.CurrentTheme = newtheme
    hook.Run("DarkUI:ThemeChanged", oldTheme, newtheme)
end

function DarkUI.GetTheme(name)
    return DarkUI.Themes[name or DarkUI.CurrentTheme] or DarkUI.Theme
end

--[[
    Basic Setup of default
]]
DarkUI.CreateTheme("Default", {})

--[[
    Testing
]]
DarkUI.CreateTheme("Random", {
    Primary = HSVToColor(math.random(0, CurTime() * 2) % 360, 1, 1),
    Background = HSVToColor(math.random(0, CurTime() * 1.2) % 360, 1, 1),
    Accent = HSVToColor(math.random(0, CurTime() * 1.6) % 360, 1, 1),
    Red = HSVToColor(math.random(0, CurTime()) % 360, 1, 1),
    Green = HSVToColor(math.random(0, CurTime()) % 360, 1, 1),
    Blue = HSVToColor(math.random(0, CurTime()) % 360, 1, 1),
    Text = {
        Default = HSVToColor(math.random(0, CurTime() * 1.1) % 360, 1, 1),
        Brighter = HSVToColor(math.random(0, CurTime() * 1.4) % 360, 1, 1),
        Bright = HSVToColor(math.random(0, CurTime() * 2) % 360, 1, 1),
        Darker = HSVToColor(math.random(0, CurTime() * 0.9) % 360, 1, 1),
        Dark = HSVToColor(math.random(0, CurTime() * .7) % 360, 1, 1)
    }
})