local PANEL = {}
AccessorFunc(PANEL, "m_iOverlap", "Overlap")
AccessorFunc(PANEL, "m_bShowDropTargets", "ShowDropTargets", FORCE_BOOL)
local _C, LerpColor = DarkLib.ModifyColor, DarkLib.LerpColor
local DrawTriangle, ScreenScale = DarkLib.DrawTriangle, DarkLib.Scale
local THEME = DarkUI.GetTheme()
local gradient_l, gradient_r = Material("vgui/gradient-l"), Material("vgui/gradient-r")

function PANEL:Init()
    THEME = DarkUI.GetTheme()
    self.color_hidden, self.color_accent = _C(THEME.Accent, .7, 0), _C(THEME.Accent, 1.7)
    self.Panels = {}
    self.OffsetX = 0
    self.TargetX = 0
    self.FrameTime = 0
    self.pnlCanvas = vgui.Create("DDragBase", self)
    self.pnlCanvas:SetDropPos("0")
    self.pnlCanvas:SetUseLiveDrag(false)

    self.pnlCanvas.OnModified = function()
        self:OnDragModified()
    end

    self.pnlCanvas.UpdateDropTarget = function(Canvas, drop, pnl)
        if (not self:GetShowDropTargets()) then return end
        DDragBase.UpdateDropTarget(Canvas, drop, pnl)
    end

    self.pnlCanvas.OnChildAdded = function(Canvas, child)
        local dn = Canvas:GetDnD()

        if (dn) then
            child:Droppable(dn)

            child.OnDrop = function()
                local x, y = Canvas:LocalCursorPos()
                local closest, id = self.pnlCanvas:GetClosestChild(x, Canvas:GetTall() / 2), 0

                for k, v in pairs(self.Panels) do
                    if (v == closest) then
                        id = k
                        break
                    end
                end

                table.RemoveByValue(self.Panels, child)
                table.insert(self.Panels, id, child)
                self:InvalidateLayout()

                return child
            end
        end
    end

    self:SetOverlap(0)
    -- TODO: better buttons
    self.btnLeft = vgui.Create("DButton", self)
    self.btnLeft:SetText("")

    self.btnLeft.Paint = function(panel, w, h)
        panel.TargetColor = panel.Hovered and self.color_accent or self.color_hidden
        panel.CurrentColor = LerpColor(FrameTime() * 10, panel.CurrentColor or self.color_hidden, panel.TargetColor)
        surface.SetMaterial(gradient_l)
        surface.SetDrawColor(THEME.Background)
        surface.DrawTexturedRect(0, 0, w, h)

        if math.Round(panel.CurrentColor.a) > 0 then
            surface.SetDrawColor(THEME.Background.r, THEME.Background.g, THEME.Background.b, panel.CurrentColor.a)
            surface.DrawRect(0, 0, w, h)
            surface.SetDrawColor(panel.CurrentColor)
            draw.NoTexture()
            DrawTriangle(w / 2, h / 2, h / 2, w / 2, -90, true)
        end
    end

    --derma.SkinHook("Paint", "ButtonLeft", panel, w, h)
    self.btnRight = vgui.Create("DButton", self)
    self.btnRight:SetText("")

    self.btnRight.Paint = function(panel, w, h)
        panel.TargetColor = panel.Hovered and self.color_accent or self.color_hidden
        panel.CurrentColor = LerpColor(FrameTime() * 10, panel.CurrentColor or self.color_hidden, panel.TargetColor)
        surface.SetMaterial(gradient_r)
        surface.SetDrawColor(THEME.Background)
        surface.DrawTexturedRect(0, 0, w, h)

        if math.Round(panel.CurrentColor.a) > 0 then
            surface.SetDrawColor(THEME.Background.r, THEME.Background.g, THEME.Background.b, panel.CurrentColor.a)
            surface.DrawRect(0, 0, w, h)
            surface.SetDrawColor(panel.CurrentColor)
            draw.NoTexture()
            DrawTriangle(w / 2, h / 2, h / 2, w / 2, 90, true)
        end
    end
    --derma.SkinHook("Paint", "ButtonRight", panel, w, h)
end

function PANEL:GetCanvas()
    return self.pnlCanvas
end

function PANEL:ScrollToChild(panel)
    -- make sure our size is all good
    self:InvalidateLayout(true)
    local x, y = self.pnlCanvas:GetChildPosition(panel)
    local w, h = panel:GetSize()
    x = x + w * 0.5
    x = x - self:GetWide() * 0.5
    self:SetScroll(x)
end

function PANEL:SetScroll(x)
    self.TargetX = x
    self:InvalidateLayout(true)
end

function PANEL:SetUseLiveDrag(bool)
    self.pnlCanvas:SetUseLiveDrag(bool)
end

function PANEL:MakeDroppable(name)
    self.pnlCanvas:MakeDroppable(name)
end

function PANEL:AddPanel(pnl)
    table.insert(self.Panels, pnl)
    pnl:SetParent(self.pnlCanvas)
    self:InvalidateLayout(true)
end

function PANEL:Clear()
    self.pnlCanvas:Clear()
    self.Panels = {}
end

function PANEL:OnMouseWheeled(dlta)
    self.TargetX = self.TargetX + dlta * ScreenScale(-35)
    self:InvalidateLayout(true)

    return true
end

function PANEL:Think()
    -- Hmm.. This needs to really just be done in one place
    -- and made available to everyone.
    local FrameRate = VGUIFrameTime() - self.FrameTime
    self.FrameTime = VGUIFrameTime()

    if (self.btnRight:IsDown()) then
        self.TargetX = self.TargetX + (ScreenScale(500) * FrameRate)
        self:InvalidateLayout(true)
    end

    if (self.btnLeft:IsDown()) then
        self.TargetX = self.TargetX - (ScreenScale(500) * FrameRate)
        self:InvalidateLayout(true)
    end

    if (dragndrop.IsDragging()) then
        local x, y = self:LocalCursorPos()

        if (x < 30) then
            self.TargetX = self.TargetX - (350 * FrameRate)
        elseif (x > self:GetWide() - 30) then
            self.TargetX = self.TargetX + (350 * FrameRate)
        end

        self:InvalidateLayout(true)
    end

    -- if self.OffsetX ~= self.TargetX then
    self.OffsetX = math.Round(Lerp(FrameTime() * 10, self.OffsetX, self.TargetX or 0), 1)
    --  end
end

function PANEL:PerformLayout()
    local w, h = self:GetSize()
    self.pnlCanvas:SetTall(h)
    local x = 0

    for k, v in ipairs(self.Panels) do
        if (not IsValid(v)) then continue end
        if (not v:IsVisible()) then continue end
        v:SetPos(x, 0)
        v:SetTall(h)

        if (v.ApplySchemeSettings) then
            v:ApplySchemeSettings()
        end

        x = x + v:GetWide() - self.m_iOverlap
    end

    self.pnlCanvas:SetWide(x + self.m_iOverlap)

    if (w < self.pnlCanvas:GetWide()) then
        self.TargetX = math.Clamp(self.TargetX, 0, self.pnlCanvas:GetWide() - self:GetWide() + 1)
    else
        self.TargetX = 0
    end

    self.pnlCanvas.x = self.OffsetX * -1
    local height = self:GetTall() / 2
    self.btnLeft:SetSize(height, height)
    self.btnLeft:Dock(LEFT)
    self.btnRight:SetSize(height, height)
    self.btnRight:Dock(RIGHT)
    self.btnLeft:SetVisible(self.pnlCanvas.x < 0)
    self.btnRight:SetVisible(self.pnlCanvas.x + self.pnlCanvas:GetWide() > self:GetWide())
end

function PANEL:OnDragModified()
    -- Override me
end

function PANEL:GenerateExample(classname, sheet, w, h)
    local scroller = vgui.Create("DarkUI_HorizontalScroller")
    scroller:Dock(TOP)
    scroller:SetHeight(64)
    scroller:DockMargin(5, 50, 5, 50)
    scroller:SetOverlap(-4)

    for i = 0, 16 do
        local img = vgui.Create("DImage", scroller)
        img:SetImage("scripted/breen_fakemonitor_1")
        scroller:AddPanel(img)
    end

    sheet:AddSheet(classname, scroller, nil, true, true)
end

derma.DefineControl("DarkUI_HorizontalScroller", "", PANEL, "Panel")