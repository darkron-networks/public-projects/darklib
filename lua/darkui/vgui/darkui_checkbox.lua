local Scale = DarkLib.Scale
local _C = DarkLib.ModifyColor
local LerpColor = DarkLib.LerpColor
local surface_SetDrawColor, FrameTime = surface.SetDrawColor, FrameTime
local surface_SetMaterial, math_ceil = surface.SetMaterial, math.ceil
local surface_DrawTexturedRect = surface.DrawTexturedRect
local draw_RoundedBox = draw.RoundedBox
local PANEL = {}
AccessorFunc(PANEL, "m_bChecked", "Checked", FORCE_BOOL)
Derma_Install_Convar_Functions(PANEL)

function PANEL:Init()
    self.THEME = DarkUI.GetTheme()

    -- TODO: Better Usage
    self.Colors = {
        Border = _C(self.THEME.Accent, .95),
        BorderDisabled = _C(self.THEME.Accent, 0.1),
        hidden_green = _C(self.THEME.Green, 0.5, 0), -- vanish
        hidden_background = _C(DarkUI.Theme.Background, 0.5, 0) -- vanish
    }

    self:SetSize(Scale(16), Scale(16))
    self:SetText("")

    -- Load Cross
    DarkLib.Images.QueueLoadMaterials({
        image = "HgMhjrI",
        func = function(mat)
            DarkLib.Materials.Cross = mat
        end
    })
end

function PANEL:Paint(w, h)
    local offset = math_ceil(Scale(2))
    local offset2 = offset * 2

    -- Border
    do
        self.BorderTargetColor = self:GetChecked() and self.Colors.hidden_background or self.Colors.Border
        self.BorderCurrentColor = LerpColor(FrameTime() * 16, self.BorderCurrentColor or self.Colors.hidden_background, self.BorderTargetColor)
        DarkLib.DrawOutlinedBoxRounded(offset2, 0, 0, w, h, offset, self.BorderCurrentColor)
    end

    offset = math_ceil(Scale(3))
    offset2 = offset * 2

    if DarkLib.Materials.Cross and not DarkLib.Materials.Cross:IsError() then
        self.TargetColor = self:GetChecked() and self.THEME.Green or self.Colors.hidden_green
        self.CurrentColor = LerpColor(FrameTime() * 30, self.CurrentColor or self.Colors.hidden_green, self.TargetColor)
        surface_SetDrawColor(self.CurrentColor)
        surface_SetMaterial(DarkLib.Materials.Cross)
        surface_DrawTexturedRect(offset, offset, w - offset2, h - offset2)
    end

    if self.Loading then
        self.LoadingColor = self.LoadingColor or _C(self.THEME.Accent, 1.3)
        DarkLib.DrawLoading(w / 2, h / 2, h - Scale(2), self.LoadingColor)
    end
end

function PANEL:IsEditing()
    return self.Depressed
end

function PANEL:SetValue(val)
    -- Tobool bugs out with "0.00"
    if (tonumber(val) == 0) then
        val = 0
    end

    val = tobool(val)
    self:SetChecked(val)
    self.m_bValue = val
    self:OnChange(val)

    if (val) then
        val = "1"
    else
        val = "0"
    end

    self:ConVarChanged(val)
end

function PANEL:DoClick()
    self:Toggle()
end

function PANEL:Toggle()
    if (not self:GetChecked()) then
        self:SetValue(true)
    else
        self:SetValue(false)
    end
end

function PANEL:OnChange(bVal)
    -- For override
end

function PANEL:Think()
    self:ConVarStringThink()
end

-- No example for this control
function PANEL:GenerateExample(class, tabs, w, h)
end

derma.DefineControl("DarkUI_CheckBox", "Simple Checkbox", PANEL, "DarkUI_Button")
--[[---------------------------------------------------------
	DCheckBoxLabel
-----------------------------------------------------------]]
local PANEL = {}
AccessorFunc(PANEL, "m_iIndent", "Indent")

function PANEL:Init()
    self.THEME = DarkUI.GetTheme()
    local Margin = Scale(2)
    self:SetTall(Scale(32))
    self:DockMargin(Margin, Margin, Margin, Margin)
    self.Button = vgui.Create("DarkUI_CheckBox", self)

    self.Button.OnChange = function(_, val)
        self:OnChange(val)
    end

    self.Label = vgui.Create("DarkUI.Label", self)
    self.Label:SetMouseInputEnabled(true)

    self.Label.DoClick = function()
        self:Toggle()
    end

    self:MirrorChild("SetConVar", self.Button)
    self:MirrorChild("SetValue", self.Button)
    self:MirrorChild("SetChecked", self.Button)
    self:MirrorChild("GetChecked", self.Button)
    --
    self:MirrorChild("SetTextColor", self.Label)
end

function PANEL:Toggle()
    if self.Button:GetDisabled() then return end
    self.Button:Toggle()
end

function PANEL:PerformLayout()
    local x = self.m_iIndent or 0
    local Size = Scale(24)
    self.Button:SetSize(Size, Size)
    self.Button:SetPos(x, math.floor((self:GetTall() - self.Button:GetTall()) / 2))
    self.Label:SizeToContents()
    self.Label:SetPos(x + self.Button:GetWide() + Scale(9), 0)
    self.Label:CenterVertical()
end

function PANEL:SizeToContents(xtrX)
    self:InvalidateLayout(true) -- Update the size of the DLabel and the X offset
    self:SetWide(self.Label.x + self.Label:GetWide())
    self:SetTall(math.max(self.Button:GetTall(), self.Label:GetTall()) + (xtraX or 0))
    self:InvalidateLayout() -- Update the positions of all children
end

function PANEL:SetText(text)
    self.Label:SetText(text)
    self:SizeToContents()
end

function PANEL:SetFont(font)
    self.Label:SetFont(font)
    self:SizeToContents()
end

function PANEL:GetText()
    return self.Label:GetText()
end

function PANEL:Paint()
end

function PANEL:OnChange(bVal)
    -- For override
end

function PANEL:SetDisabled(bool)
    self.Button:SetDisabled(bool)
    self.Label:SetTextColor(self.THEME.Text.Darker)
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local PAN = vgui.Create("DarkUI_Panel")
    PAN:SetSize(300, 200)
    -- Enabled
    local ctrl = vgui.Create(ClassName, PAN)
    ctrl:SetText("CheckBox")
    ctrl:Dock(TOP)
    -- Disabled
    local ctrl = vgui.Create(ClassName, PAN)
    ctrl:SetText("Disabled CheckBox")
    ctrl:SetDisabled(true)
    ctrl:Dock(TOP)
    local ctrl = vgui.Create(ClassName, PAN)
    ctrl:SetText("Disabled CheckBox (Checked)")
    ctrl:SetDisabled(true)
    ctrl:SetChecked(true)
    ctrl:Dock(TOP)
    PropertySheet:AddSheet(ClassName, PAN, nil, true, true)
end

derma.DefineControl("DarkUI_CheckBoxLabel", "Simple Checkbox", PANEL, "DarkUI_Panel")