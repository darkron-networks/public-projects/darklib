--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local PANEL = {}
local DrawTriangle, FrameTime = DarkLib.DrawTriangle, FrameTime
local ScreenScale = DarkLib.Scale
local c = DarkLib.ModifyColor
AccessorFunc(PANEL, "m_bSmoothFactor", "AnimationTime", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bScrollFactor", "ScrollFactor", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bSmoothed", "Smooth", FORCE_BOOL)

function PANEL:Init()
    -- Color
    self.THEME = DarkUI.GetTheme()

    self.Colors = {
        Background = Color(0, 0, 0, 50),
        Bar = c(self.THEME.Accent, 0.8), -- Color(77, 77, 77, 190),
        BarHover = c(self.THEME.Accent, 0.9), -- Color(77, 77, 77, 230),
        Button = c(self.THEME.Accent, 0.8, 190),
        ButtonHover = c(self.THEME.Accent, 0.9)
    }

    -- Defaults
    self.CurrentY = 0
    self.TargetY = 0
    --self.m_bSmoothFactor = 0.15
    self.m_bSmoothed = true
    self.m_bScrollFactor = 2
    self:SetWide(ScreenScale(10))
    local offsetBorder = math.ceil(ScreenScale(1))
    local offsetBorder2 = math.ceil(offsetBorder * 2)

    self.btnGrip.Paint = function(btn, w, h)
        draw.RoundedBox(w / 4, offsetBorder, offsetBorder, w - offsetBorder2, h - offsetBorder2, btn:IsHovered() and self.Colors.BarHover or self.Colors.Bar)
    end

    self.btnGrip:SetCursor("hand")

    self.btnUp.Paint = function(btn, w, h)
        surface.SetDrawColor(self:IsHovered() and self.Colors.ButtonHover or btn.Colors.Button)
        draw.NoTexture()
        DrawTriangle(w - (w / 2), h - (h / 2), w, h)
    end

    self.btnDown.Paint = function(btn, w, h)
        surface.SetDrawColor(self:IsHovered() and self.Colors.ButtonHover or btn.Colors.Button)
        draw.NoTexture()
        DrawTriangle(w - (w / 2), h - (h / 2), w, h, 180)
    end

    self.btnUp.DoClick = function(self)
        self:GetParent():AddScroll(self.m_bScrollFactor * -1)
    end

    self.btnDown.DoClick = function(self)
        self:GetParent():AddScroll(self.m_bScrollFactor)
    end

    self:SetHideButtons(true)
    self:InvalidateLayout(true)
end

function PANEL:Paint(w, h)
    draw.RoundedBox(w / 4, 0, 0, w, h, self.Colors.Background)
end

function PANEL:Think()
    if self.m_bSmoothed and self.CurrentY ~= self.TargetY then
        self.CurrentY = Lerp(self.m_bSmoothFactor or (FrameTime() * 10), self.CurrentY, self.TargetY)
        self.btnGrip:SetPos(0, self.CurrentY)
    end
end

function PANEL:IsDragged()
    -- if self.btnGrip:IsDown() then return true end
    if self.Dragging then return true end
    if self:IsHovered() then return true end
    if self.btnGrip:IsHovered() then return true end
    if self.btnUp:IsHovered() then return true end
    if self.btnDown:IsHovered() then return true end

    return false
end

function PANEL:PerformLayout()
    local Wide = self:GetWide()
    local BtnHeight = Wide

    if (self:GetHideButtons()) then
        BtnHeight = 0
    end

    local Scroll = self:GetScroll() / self.CanvasSize
    local BarSize = math.max(self:BarScale() * (self:GetTall() - (BtnHeight * 2)), 10)
    local Track = self:GetTall() - (BtnHeight * 2) - BarSize
    Track = Track + 1
    Scroll = Scroll * Track

    if self.m_bSmoothed then
        self.TargetY = BtnHeight + Scroll
    else
        self.btnGrip:SetPos(0, BtnHeight + Scroll)
    end

    self.btnGrip:SetSize(Wide, BarSize)

    if (BtnHeight > 0) then
        self.btnUp:SetPos(0, 0, Wide, Wide)
        self.btnUp:SetSize(Wide, BtnHeight)
        self.btnDown:SetPos(0, self:GetTall() - BtnHeight)
        self.btnDown:SetSize(Wide, BtnHeight)
        self.btnUp:SetVisible(true)
        self.btnDown:SetVisible(true)
    else
        self.btnUp:SetVisible(false)
        self.btnDown:SetVisible(false)
        self.btnDown:SetSize(Wide, BtnHeight)
        self.btnUp:SetSize(Wide, BtnHeight)
    end
end

derma.DefineControl("DarkUI_ScrollBar", "DarkUI Scrollbar", PANEL, "DVScrollBar")