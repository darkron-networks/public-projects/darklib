--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
    TODO: optimize
]]
local PANEL = {}
AccessorFunc(PANEL, "m_bTranslatePlaceholder", "TranslatePlaceholder")
AccessorFunc(PANEL, "m_splaceholder", "Placeholder")
local ScreenScale = DarkLib.Scale
local GetPhrase = language.GetPhrase
local draw_SimpleText = draw.SimpleText
local string_StartWith, string_sub = string.StartWith, string.sub

function PANEL:Init()
    self:SetTall(ScreenScale(20))
    self:SetDrawLanguageID(false)
    self:SetFont("DarkUI.Text")
end

function PANEL:Paint(w, h)
    if not self.NoBackground then
        if self:GetDisabled() then
            draw.RoundedBox(0, 0, 0, w, h, self._mBackgroundDisabled or DarkUI.Colors.DTextEntry.background.disabled)
        else
            draw.RoundedBox(0, 0, 0, w, h, self._mBackgroundEnabled or DarkUI.Colors.DTextEntry.background.enabled)
        end
    end

    local col = self:GetTextColor()
    self:DrawTextEntryText(col, self:GetHighlightColor(), col)

    if (#self:GetText() == 0) then
        local placholder = self.m_splaceholder or ""
        if placeholder == "" then return end

        if self.m_bTranslatePlaceholder then
            if string_StartWith(placholder, "#") then
                placholder = string_sub(placholder, 2)
            end

            placholder = GetPhrase(placholder)
        end

        draw_SimpleText(placholder, self:GetFont(), 3, self:IsMultiline() and ScreenScale(9.5) or h / 2, self:GetPlaceholderColor(), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
    end
end

-- Color Setters
function PANEL:SetBackgroundColor(enabled, disabled)
    self._mBackgroundEnabled = IsColor(enabled) and enabled or nil
    self._mBackgroundDisabled = IsColor(disabled) and disabled or nil
end

function PANEL:SetScrollbar(bool)
    -- TODO: make own implementation?
    self:SetVerticalScrollbarEnabled(bool)
end

function PANEL:SetTextColor(color)
    self._mColText = IsColor(color) and color or nil
end

function PANEL:SetHighlightColor(color)
    self.m_colHighlight = IsColor(color) and color or nil
end

function PANEL:SetCursorColor(color)
    self.m_colCursor = IsColor(color) and color or nil
end

-- Color Getters
function PANEL:GetTextColor()
    return self.m_colText or DarkUI.Colors.DTextEntry.textColor
end

function PANEL:GetPlaceholderColor()
    return self.m_colPlaceholder or DarkUI.Colors.DTextEntry.placeholderTextColor
end

function PANEL:GetHighlightColor()
    return self.m_colHighlight or DarkUI.Colors.DTextEntry.highlightColor
end

function PANEL:GetCursorColor()
    return self.m_colCursor or DarkUI.Colors.DTextEntry.cursorColor
end

-- Just in case someone wants to replace the old textentry
function PANEL:SetPlaceholderText(str)
    self.m_splaceholder = str
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local ctrl = vgui.Create(ClassName)
    ctrl:SetText("Edit Me!")
    ctrl:SetPlaceholder("Placeholder!")
    ctrl:SetWide(150)

    ctrl.OnEnter = function(self)
        Derma_Message("You Typed: " .. self:GetValue())
    end

    PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
end

derma.DefineControl("DarkUI_TextEntry", "DarkUI Test input", PANEL, "DTextEntry")