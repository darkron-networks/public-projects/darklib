--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local Flat_Colors = {
    Dark = Color(33, 33, 33),
    Gray = Color(102, 102, 102),
    LightGray = Color(119, 119, 119),
    DarkGray = Color(85, 85, 85),
    Text = Color(255, 255, 255),
    TextAlpha = Color(255, 255, 255, 166),
    Blue = Color(21, 101, 192),
    Green = Color(67, 160, 71),
    Red = Color(213, 0, 0),
    LightRed = Color(255, 82, 82),
    DarkRed = Color(183, 28, 28)
}

local Colors = {
    DListView = {
        background = Color(50, 50, 50),
        backgroundAlpha = Color(50, 50, 50, 240),
        Header = {
            default = Color(0, 0, 0),
            Hover = Color(10, 10, 10)
        },
        Item = {
            default = Color(0, 0, 0),
            Hover = Color(10, 10, 10)
        }
    },
    DListView_Line = {
        background = Color(0, 0, 0, 100),
        backgroundAlt = Color(0, 0, 0, 150),
        Hover = Color(51, 65, 92),
        Selected = Color(2, 62, 125)
    }
}

local Center = DarkLib.Center
local c = DarkLib.ModifyColor
local ScreenScale = DarkLib.Scale
--[[------------------------------------------
    DListViewLabel
--------------------------------------------]]
local PANEL = {}
PANEL.GenerateExample = function() end

function PANEL:Init()
    self:SetFont("DarkUI.Text")
    self:SetTextInset(ScreenScale(6), Center(self:GetTall()))
    self:InvalidateLayout(true)
end

derma.DefineControl("DarkUI_ListViewLabel", "", PANEL, "DLabel")
--[[------------------------------------------
    DListView_ColumnPlain
--------------------------------------------]]
local PANEL = {}

function PANEL:Init()
    self.Header:Remove()
    self.Header = vgui.Create("DarkUI_Button", self)
    self.Header.DoClick = function() end
    self.Header.DoRightClick = function() end
    self.Header:SetDisabled(true)
    self.Header:SetDisabledColor(c(DarkUI.Theme.Accent, 0.8))
end

derma.DefineControl("DarkUI_ListView_ColumnPlain", "A simple panel", PANEL, "DListView_ColumnPlain")
--[[------------------------------------------
    DListView_Column
--------------------------------------------]]
local PANEL = {}

function PANEL:Init()
    self.Header:Remove()
    self.Header = vgui.Create("DarkUI_Button", self)

    self.Header.DoClick = function()
        self:DoClick()
    end

    self.Header.DoRightClick = function()
        self:DoRightClick()
    end
end

derma.DefineControl("DarkUI_ListView_Column", "A simple panel", PANEL, "DListView_Column")
--[[------------------------------------------
    DListView_Line
--------------------------------------------]]
local PANEL = {}

function PANEL:Init()
    -- Default
end

function PANEL:Paint(w, h)
    if (self:IsSelected()) then
        draw.RoundedBox(0, 0, 0, w, h, Colors.DListView_Line.Selected)
    elseif (self.Hovered) then
        draw.RoundedBox(0, 0, 0, w, h, Colors.DListView_Line.Hover)
    elseif (self.m_bAlt) then
        draw.RoundedBox(0, 0, 0, w, h, Colors.DListView_Line.backgroundAlt)
    elseif Colors.DListView_Line.background then
        draw.RoundedBox(0, 0, 0, w, h, Colors.DListView_Line.background)
    end
end

function PANEL:SetColumnText(i, strText)
    if (type(strText) == "Panel") then
        if (IsValid(self.Columns[i])) then
            self.Columns[i]:Remove()
        end

        strText:SetParent(self)
        self.Columns[i] = strText
        self.Columns[i].Value = strText

        return
    end

    if (not IsValid(self.Columns[i])) then
        self.Columns[i] = vgui.Create("DarkUI_ListViewLabel", self)
        self.Columns[i]:SetMouseInputEnabled(false)
    end

    self.Columns[i]:SetText(tostring(strText))
    self.Columns[i]:SetTextColor(Flat_Colors.TextAlpha)
    self.Columns[i].Value = strText

    if self._Align then
        self.Columns[i]:SetContentAlignment(self._Align)
    end

    return self.Columns[i]
end

PANEL.SetValue = PANEL.SetColumnText

function PANEL:SetAlign(align)
    local aligns = {
        ["bottom-left"] = 1,
        ["bottom-center"] = 2,
        ["bottom-right"] = 3,
        ["middle-left"] = 4,
        ["center"] = 5,
        ["middle-right"] = 6,
        ["top-left"] = 7,
        ["top-center"] = 8,
        ["top-right"] = 9
    }

    self._Align = aligns[align or ""]
end

function PANEL:DataLayout(ListView)
    self:ApplySchemeSettings()
    local height = self:GetTall()
    local x = 0

    for k, Column in pairs(self.Columns) do
        local w = ListView:ColumnWidth(k)
        Column:SetPos(x, 0)
        Column:SetSize(w, height)
        x = x + w
    end
end

derma.DefineControl("DarkUI_ListView_Line", "A simple panel", PANEL, "DListView_Line")
--[[------------------------------------------
    DListView
--------------------------------------------]]
local PANEL = {}
PANEL.AddLineQueue = {}

function PANEL:Init()
    self.pnlCanvas.CurrentOffset = 0
    self.pnlCanvas.TargetOffset = 0
    self.m_bSmoothed = true

    --self.m_bSmoothFactor = 0.15
    function self.pnlCanvas.Think(cns)
        if self.m_bSmoothed and cns.CurrentOffset ~= cns.TargetOffset then
            cns.CurrentOffset = Lerp(self.m_bSmoothFactor or (FrameTime() * 10), cns.CurrentOffset, cns.TargetOffset)

            if (self.m_bHideHeaders) then
                cns:SetPos(0, cns.CurrentOffset)
            else
                cns:SetPos(0, cns.CurrentOffset + self:GetHeaderHeight())
            end
        end
    end

    self.VBar:Remove()
    self.VBar = vgui.Create("DarkUI_ScrollBar", self)
    self.VBar:SetZPos(ScreenScale(20))
    self:SetHeaderHeight(ScreenScale(20))
    self:SetDataHeight(ScreenScale(20))
end

function PANEL:OnVScroll(iOffset)
    if self.m_bSmoothed then
        self.pnlCanvas.TargetOffset = iOffset
    else
        self.pnlCanvas:SetPos(0, iOffset)
    end
end

function PANEL:Paint(w, h)
    --draw.RoundedBox(0, 0, 0, w, h, Colors.DListView.background)
end

function PANEL:SetItemAlign(align)
    self._ItemAlign = align
end

function PANEL:AddColumn(strName, iPosition)
    local pColumn = nil

    if (self.m_bSortable) then
        pColumn = vgui.Create("DarkUI_ListView_Column", self)
    else
        pColumn = vgui.Create("DarkUI_ListView_ColumnPlain", self)
    end

    pColumn:SetName(strName)
    pColumn:SetZPos(10)

    if (iPosition) then
        table.insert(self.Columns, iPosition, pColumn)

        for i = 1, #self.Columns do
            self.Columns[i]:SetColumnID(i)
        end
    else
        local ID = table.insert(self.Columns, pColumn)
        pColumn:SetColumnID(ID)
    end

    self:InvalidateLayout()

    return pColumn
end

function PANEL:AddLine(...)
    self:SetDirty(true)
    self:InvalidateLayout()
    local Line = vgui.Create("DarkUI_ListView_Line", self.pnlCanvas)
    local ID = table.insert(self.Lines, Line)

    if self._ItemAlign and isstring(self._ItemAlign) then
        Line:SetAlign(self._ItemAlign)
    end

    Line:SetListView(self)
    Line:SetID(ID)

    -- This assures that there will be an entry for every column
    for k, v in pairs(self.Columns) do
        Line:SetColumnText(k, "")
    end

    for k, v in pairs({...}) do
        Line:SetColumnText(k, v)
    end

    -- Make appear at the bottom of the sorted list
    local SortID = table.insert(self.Sorted, Line)

    if (SortID % 2 == 1) then
        Line:SetAltLine(true)
    end

    return Line
end

-- Can we put it in the scrollbar itself?
function PANEL:Think()
    local vbar = self.VBar
    local canvasHeight = self.pnlCanvas:GetTall()
    YPos = vbar:GetOffset()
    local totalOffset = -1 * YPos + vbar:GetTall()

    if totalOffset >= canvasHeight then
        vbar:SetScroll(canvasHeight)
    end
end

function PANEL:PerformLayout()
    -- Do Scrollbar
    local Wide = self:GetWide()
    local YPos = 0

    if (IsValid(self.VBar)) then
        local HeaderEnabled = not self:GetHideHeaders()
        local barWith = self.VBar:GetWide()
        self.VBar:SetPos(self:GetWide() - barWith, 0)
        self.VBar:SetSize(barWith, self:GetTall())
        self.VBar:SetUp(self.VBar:GetTall() - (HeaderEnabled and self:GetHeaderHeight() or 0), self.pnlCanvas:GetTall())
        YPos = self.VBar:GetOffset()

        if (self.VBar.Enabled) then
            Wide = Wide - barWith
        end
    end

    if (self.m_bHideHeaders) then
        self.pnlCanvas:SetPos(-1, YPos)
    else
        self.pnlCanvas:SetPos(-1, YPos + (HeaderEnabled and self:GetHeaderHeight() or 0))
    end

    self.pnlCanvas:SetSize(Wide, self.pnlCanvas:GetTall())
    self:FixColumnsLayout()

    --
    -- If the data is dirty, re-layout
    --
    if (self:GetDirty()) then
        self:SetDirty(false)
        local y = self:DataLayout()
        self.pnlCanvas:SetTall(y)
        -- Layout again, since stuff has changed..
        self:InvalidateLayout(true)
    end
end

function PANEL:SetAnimationTime(time)
    assert(isnumber(time), "SetAnimationTime only accepts a Number value.")
    self.m_bSmoothFactor = time
    self.VBar:SetAnimationTime(time)
end

function PANEL:SetSmooth(bool)
    assert(isbool(bool), "SetSmooth only accepts a Bool value.")
    self.m_bSmoothed = bool
    self.VBar:SetSmooth(bool)
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local ctrl = vgui.Create(ClassName)
    local Col1 = ctrl:AddColumn("Address")
    local Col2 = ctrl:AddColumn("Port")
    Col2:SetMinWidth(30)
    Col2:SetMaxWidth(30)

    for i = 1, 128 do
        ctrl:AddLine("192.168.0." .. i, "80")
    end

    ctrl:SetSize(300, 200)
    PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
end

derma.DefineControl("DarkUI_ListView", "DarkUI ListView", PANEL, "DListView")