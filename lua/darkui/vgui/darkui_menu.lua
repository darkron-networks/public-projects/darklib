local _S = DarkLib.Scale
local c = DarkLib.ModifyColor
local DrawTriangle = DarkLib.DrawTriangle
local surface_SetDrawColor = surface.SetDrawColor
local draw_NoTexture = draw.NoTexture

--[[
    DMenuOption component
--]]
do
    local PANEL = {}
    AccessorFunc(PANEL, "m_pMenu", "Menu")
    AccessorFunc(PANEL, "m_bChecked", "Checked")
    AccessorFunc(PANEL, "m_bCheckable", "IsCheckable")

    function PANEL:Init()
        self.BaseClass.Init(self)
        self:SetContentAlignment(_S(4))
        self:SetTextInset(_S(6), 0) -- Room for icon on left
        self:SetChecked(false)
        self:SetAutoStretchVertical(true)
        self:SetFont("DarkUI.TextSmall")
    end

    function PANEL:SetSubMenu(menu)
        self.SubMenu = menu
    end

    function PANEL:AddSubMenu()
        local SubMenu = DarkUIMenu(self)
        SubMenu:SetVisible(false)
        SubMenu:SetParent(self)
        self:SetSubMenu(SubMenu)

        return SubMenu
    end

    function PANEL:OnCursorEntered()
        if (IsValid(self.ParentMenu)) then
            self.ParentMenu:OpenSubMenu(self, self.SubMenu)

            return
        end

        self:GetParent():OpenSubMenu(self, self.SubMenu)
    end

    function PANEL:OnCursorExited()
    end

    function PANEL:Paint(w, h)
        derma.SkinHook("Paint", "MenuOption", self, w, h)

        if self.SubMenu then
            local size = h / 3
            surface_SetDrawColor(DarkUI.Theme.Accent)
            draw_NoTexture()
            DrawTriangle(self:GetParent():GetWide() - size, h - (h / 2), size, size, 90, true)
        end

        return false
    end

    function PANEL:OnMousePressed(mousecode)
        self.m_MenuClicking = true
        DButton.OnMousePressed(self, mousecode)
    end

    function PANEL:OnMouseReleased(mousecode)
        DButton.OnMouseReleased(self, mousecode)

        if (self.m_MenuClicking and mousecode == MOUSE_LEFT) then
            self.m_MenuClicking = false
            CloseDermaMenus()
        end
    end

    function PANEL:DoRightClick()
        if (self:GetIsCheckable()) then
            self:ToggleCheck()
        end
    end

    function PANEL:DoClickInternal()
        if (self:GetIsCheckable()) then
            self:ToggleCheck()
        end

        if (self.m_pMenu) then
            self.m_pMenu:OptionSelectedInternal(self)
        end
    end

    function PANEL:ToggleCheck()
        self:SetChecked(not self:GetChecked())
        self:OnChecked(self:GetChecked())
    end

    function PANEL:OnChecked(b)
    end

    function PANEL:PerformLayout(w, h)
        self:SizeToContentsX(_S(30))
        self:SizeToContentsY(_S(12))
        local w = math.max(self:GetParent():GetWide(), self:GetWide())
        self:SetWide(w)
        self.BaseClass.PerformLayout(self, w, h)
    end

    function PANEL:GenerateExample()
        -- Do nothing!
    end

    derma.DefineControl("DarkUI_MenuOption", "Menu Option Line", PANEL, "DarkUI_Button")
end

--[[
    Main Menu component
--]]
do
    local PANEL = {}
    AccessorFunc(PANEL, "m_bBorder", "DrawBorder")
    AccessorFunc(PANEL, "m_bDeleteSelf", "DeleteSelf")
    AccessorFunc(PANEL, "m_iMinimumWidth", "MinimumWidth")
    AccessorFunc(PANEL, "m_iMaximumWidth", "MaximumWidth")
    AccessorFunc(PANEL, "m_bDrawColumn", "DrawColumn")
    AccessorFunc(PANEL, "m_iMaxHeight", "MaxHeight")
    AccessorFunc(PANEL, "m_pOpenSubMenu", "OpenSubMenu")

    function PANEL:Init()
        self:SetIsMenu(true)
        self:SetDrawBorder(true)
        self:SetPaintBackground(true)
        self:SetBackgroundColor(c(DarkUI.Theme.Background, 1.4))
        self:SetMinimumWidth(_S(100))
        self:SetMaximumWidth(_S(350))
        self:SetDrawOnTop(true)
        self:SetMaxHeight(_S(300))
        self:SetDeleteSelf(true)
        self:SetPadding(0)
        self:SetSmooth(false) -- TODO: Currently not working correctly
        -- Automatically remove this panel when menus are to be closed
        RegisterDermaMenuForClose(self)
    end

    function PANEL:AddPanel(pnl)
        self:AddItem(pnl)
        pnl.ParentMenu = self
    end

    -- TODO: custom
    function PANEL:AddOption(strText, funcFunction)
        local pnl = vgui.Create("DarkUI_MenuOption", self)
        pnl:SetMenu(self)
        pnl:SetText(strText)

        if (funcFunction) then
            pnl.DoClick = funcFunction
        end

        self:AddPanel(pnl)

        return pnl
    end

    -- TODO: custom
    function PANEL:AddCVar(strText, convar, on, off, funcFunction)
        local pnl = vgui.Create("DMenuOptionCVar", self)
        pnl:SetMenu(self)
        pnl:SetText(strText)

        if (funcFunction) then
            pnl.DoClick = funcFunction
        end

        pnl:SetConVar(convar)
        pnl:SetValueOn(on)
        pnl:SetValueOff(off)
        self:AddPanel(pnl)

        return pnl
    end

    -- TODO: custom
    function PANEL:AddSpacer(strText, funcFunction)
        local pnl = vgui.Create("DarkUI_Panel", self)

        pnl.Paint = function(p, w, h)
            derma.SkinHook("Paint", "MenuSpacer", p, w, h)
        end

        pnl:SetTall(1)
        self:AddPanel(pnl)

        return pnl
    end

    -- TODO: custom
    function PANEL:AddSubMenu(strText, funcFunction)
        local pnl = vgui.Create("DarkUI_MenuOption", self)
        local SubMenu = pnl:AddSubMenu(strText, funcFunction)
        pnl:SetText(strText)

        if (funcFunction) then
            pnl.DoClick = funcFunction
        end

        self:AddPanel(pnl)

        return SubMenu, pnl
    end

    function PANEL:Hide()
        local openmenu = self:GetOpenSubMenu()

        if (openmenu) then
            openmenu:Hide()
        end

        self:SetVisible(false)
        self:SetOpenSubMenu(nil)
    end

    function PANEL:OpenSubMenu(item, menu)
        -- Do we already have a menu open?
        local openmenu = self:GetOpenSubMenu()

        if (IsValid(openmenu) and openmenu:IsVisible()) then
            -- Don't open it again!
            if (menu and openmenu == menu) then return end
            -- Close it!
            self:CloseSubMenu(openmenu)
        end

        if (not IsValid(menu)) then return end
        local x, y = item:LocalToScreen(self:GetWide(), 0)
        menu:Open(x, y, false, item)
        self:SetOpenSubMenu(menu)
    end

    function PANEL:CloseSubMenu(menu)
        menu:Hide()
        self:SetOpenSubMenu(nil)
    end

    function PANEL:ChildCount()
        return #self:GetCanvas():GetChildren()
    end

    function PANEL:GetChild(num)
        return self:GetCanvas():GetChildren()[num]
    end

    function PANEL:PerformLayout(w, h)
        local w = self:GetMinimumWidth()
        local maxW = self.m_iMaximumWidth

        -- Find the widest one
        for k, pnl in pairs(self:GetCanvas():GetChildren()) do
            pnl:InvalidateLayout(true)
            w = math.max(w, pnl:GetWide())
        end

        w = math.min(maxW, w)
        self:SetWide(w)

        for k, pnl in pairs(self:GetCanvas():GetChildren()) do
            pnl:SetWide(w)
            pnl:Dock(TOP)
            pnl:InvalidateLayout()
        end

        local height = math.min(self:GetCanvas():GetTall(), self:GetMaxHeight())
        self:SetTall(height)
        --derma.SkinHook("Layout", "Menu", self)
        self.BaseClass.PerformLayout(self, w, h)
        --DarkUI_ScrollPanel.PerformLayout(self)
    end

    --Open - Opens the menu.
    --x and y are optional, if they're not provided the menu
    --	will appear at the cursor.
    function PANEL:Open(x, y, skipanimation, ownerpanel)
        RegisterDermaMenuForClose(self)
        local maunal = x and y
        x = x or gui.MouseX()
        y = y or gui.MouseY()
        local OwnerHeight = 0
        local OwnerWidth = 0

        if (ownerpanel) then
            OwnerWidth, OwnerHeight = ownerpanel:GetSize()
        end

        self:InvalidateLayout(true)
        local w = self:GetWide()
        local h = self:GetTall()

        --self:SetSize(w, h)
        if (y + h > ScrH()) then
            -- TODO: Make it smaller if we go over ScrH until it reaches MinHeight
            y = ((maunal and ScrH()) or (y + OwnerHeight)) - h
        end

        if (x + w > ScrW()) then
            x = ((maunal and ScrW()) or x) - w
        end

        if (y < 1) then
            y = 1
        end

        if (x < 1) then
            x = 1
        end

        self:SetPos(x, y)
        -- Popup!
        self:MakePopup()
        -- Make sure it's visible!
        self:SetVisible(true)
        -- Keep the mouse active while the menu is visible.
        self:SetKeyboardInputEnabled(false)
    end

    --
    -- Called by DMenuOption
    --
    function PANEL:OptionSelectedInternal(option)
        self:OptionSelected(option, option:GetText())
    end

    function PANEL:OptionSelected(option, text)
        -- For override
    end

    function PANEL:ClearHighlights()
        for k, pnl in pairs(self:GetCanvas():GetChildren()) do
            pnl.Highlight = nil
        end
    end

    function PANEL:HighlightItem(item)
        for k, pnl in pairs(self:GetCanvas():GetChildren()) do
            if (pnl == item) then
                pnl.Highlight = true
            end
        end
    end

    function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
        local MenuItemSelected = function()
            Derma_Message("Choosing a menu item worked!")
        end

        local ctrl = vgui.Create("DarkUI_Button")
        ctrl:SetText("Test Me!")

        ctrl.DoClick = function()
            local menu = DarkUIMenu()
            menu:AddOption("Option One", MenuItemSelected)
            menu:AddOption("Option 2", MenuItemSelected)
            local submenu = menu:AddSubMenu("Option Free")
            submenu:AddOption("Submenu 1", MenuItemSelected)
            submenu:AddOption("Submenu 2", MenuItemSelected):SetIcon("icon16/bug.png") -- in case i want to add support for it again
            menu:AddOption("Option For", MenuItemSelected)
            menu:Open()
        end

        PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
    end

    derma.DefineControl("DarkUI_Menu", "A Menu", PANEL, "DarkUI_ScrollPanel")
end

--[[
    Global functions/helpers
--]]
-- Own Menu "generator"
function DarkUIMenu(parentmenu, parent)
    if (not parentmenu) then
        CloseDermaMenus()
    end

    local dmenu = vgui.Create("DarkUI_Menu", parent)

    return dmenu
end