local PANEL = {}
local NULLFUNC = function() end
AccessorFunc(PANEL, "m_bCornersRadius", "CornerRadius", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bHeaderHeight", "HeaderHeight", FORCE_NUMBER)

local Colors = {
    background = Color(50, 50, 50),
    backgroundAlpha = Color(50, 50, 50, 240),
    header = Color(33, 33, 33),
    headerAlpha = Color(33, 33, 33, 240)
}

function PANEL:Init()
    self.m_bHeaderHeight = 24
    self:InvalidateLayout()

    --[[
            Button Replacement
    --]]
    if IsValid(self.btnClose) then
        self.btnClose:Remove()
    end

    if IsValid(self.btnMaxim) then
        self.btnMaxim:Remove()
    end

    if IsValid(self.btnMinim) then
        self.btnMinim:Remove()
    end

    -- Button Panel
    self.btnPanel = vgui.Create("DarkUI_Panel", self)
    -- Close button
    self.btnClose = vgui.Create("DarkUI_Button", self.btnPanel)
    self.btnClose:Dock(RIGHT)
    self.btnClose:SetFont("DarkUI.FrameButton")
    self.btnClose:SetText("✕")
    self.btnClose:SizeToContents()

    self.btnClose.DoClick = function(button)
        self:Close()
    end

    self.btnMaxim = vgui.Create("DarkUI_Button", self.btnPanel)
    self.btnMaxim:Dock(RIGHT)
    self.btnMaxim:SetFont("DarkUI.FrameButton")
    self.btnMaxim:SetText(utf8.char(2800))

    self.btnMaxim.DoClick = function(button)
        self:Close()
    end

    self.btnMinim = vgui.Create("DarkUI_Button", self.btnPanel)
    self.btnMinim:Dock(RIGHT)
    self.btnMinim:SetFont("DarkUI.FrameButton")
    self.btnMinim:SetText("＿")

    self.btnMinim.DoClick = function(button)
        self:Close()
    end

    self.btnClose.Paint = NULLFUNC
    self.btnMaxim.Paint = NULLFUNC
    self.btnMinim.Paint = NULLFUNC
    self.btnMaxim:SetDisabled(true)
    self.btnMinim:SetDisabled(true)
    self.btnMaxim:Hide(true)
    self.btnMinim:Hide(true)
end

function PANEL:Paint(w, h)
    local backgroundColor = self._Transparent and Colors.backgroundAlpha or Colors.background
    local headerColor = self._Transparent and Colors.headerAlpha or Colors.header
    draw.RoundedBox(self.m_bCornersRadius or 4, 0, 0, w, h, backgroundColor)
    draw.RoundedBoxEx(self.m_bCornersRadius or 4, 0, 0, w, self.m_bHeaderHeight, headerColor, self.m_bCornersRadius or 4, self.m_bCornersRadius or 4)

    if self._backgroundMaterial then
        local mode = self._backgroundMaterialMode or "stretch"
        surface.SetDrawColor(255, 255, 255, 100)
        surface.SetMaterial(self._backgroundMaterial)

        if mode == "stretch" then
            surface.DrawTexturedRect(0, 20, w, h)
        elseif mode == "center" then
            surface.DrawTexturedRect((w / 2) - (h / 2), 20, h, h - 30)
        end
    end

    if (self.m_bBackgroundBlur) then
        Derma_DrawBackgroundBlur(self, self.m_fCreateTime)
    end

    return false
end

--[[
        Remove child frames on removal
    ]]
PANEL.oldRemove = PANEL.OnRemove or function() end

function PANEL:OnRemove(...)
    for i, frame in ipairs(self._toRemoveOnRemove or {}) do
        if IsValid(frame) then
            frame:Remove()
        end
    end

    return self.oldRemove and self:oldRemove(...) or nil
end

function PANEL:SetBackgroundMaterial(material, mode)
    if isstring(material) and type(material) ~= "IMaterial" then
        material = Material(material, "noclamp smooth")
    end

    self._backgroundMaterial = material
    self._backgroundMaterialMode = mode
end

function PANEL:SetParentFrame(frame)
    if IsValid(frame) and frame:GetName() == "DFrame" and self:GetName() == "DFrame" then
        frame._toRemoveOnRemove = frame._toRemoveOnRemove or {}
        table.insert(frame._toRemoveOnRemove, self)
    end
end

function PANEL:SetTransparent(enable)
    self._Transparent = enable
end

function PANEL:SetMinimizeAction(callback)
    if isfunction(callback) then
        self.btnMinim:Show()
        self.btnMinim.DoClick = callback
        self.btnMinim:SetDisabled(false)
    else
        self.btnMinim:Hide()
        self.btnMinim:SetDisabled(true)
    end

    self:InvalidateLayout(true)
end

function PANEL:SetMaximizeAction(callback)
    if isfunction(callback) then
        self.btnMaxim:Show()
        self.btnMaxim.DoClick = callback
        self.btnMaxim:SetDisabled(false)
    else
        self.btnMaxim:Hide()
        self.btnMaxim:SetDisabled(true)
    end

    self:InvalidateLayout(true)
end

function PANEL:PerformLayout(w, h)
    local headerHeight = self.m_bHeaderHeight
    self:DockPadding(2, headerHeight + 2, 2, 2)

    if isnumber(w) and w < self:GetMinWidth() then
        self:SetWide(self:GetMinWidth())
    end

    if isnumber(h) and h < self:GetMinHeight() then
        self:SetTall(self:GetMinHeight())
    end

    local titlePush = 0

    if (IsValid(self.imgIcon)) then
        self.imgIcon:SetPos(5, 5)
        self.imgIcon:SetSize(16, 16)
        titlePush = 16
    end

    self.btnClose:SetPos(self:GetWide() - 31 - 4, 0)
    self.btnClose:SetSize(headerHeight, headerHeight)
    self.btnMaxim:SetPos(self:GetWide() - 31 * 2 - 4, 0)
    self.btnMaxim:SetSize(headerHeight, headerHeight)
    self.btnMinim:SetPos(self:GetWide() - 31 * 3 - 4, 0)
    self.btnMinim:SetSize(headerHeight, headerHeight)
    self.lblTitle:SetPos(8 + titlePush, 2)
    self.lblTitle:SetSize(self:GetWide() - 25 - titlePush, 20)
    -- Butten Panel setup
    local wide = 0

    if self.btnClose:IsVisible() then
        wide = wide + 22
    end

    if self.btnMinim:IsVisible() then
        wide = wide + 22
    end

    if self.btnMaxim:IsVisible() then
        wide = wide + 22
    end

    if wide > 0 then
        if wide == 22 then
            wide = wide + 4
        end

        if wide == 44 then
            wide = wide + 12
        end

        if wide == 66 then
            wide = wide + 22
        end

        self.btnPanel:Show()
        self.btnPanel:SetPos(self:GetWide() - wide + -(self.btnPanel:GetWide() / 2), headerHeight / 2 - (self.btnPanel:GetWide() / 2))
        self.btnPanel:SetSize(wide, 24)
    else
        self.btnPanel:Hide()
    end
end

derma.DefineControl("DarkUI_Frame_old", "Main Frame for stuff", PANEL, "DFrame")