--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local PANEL = {}
local ScreenScale = DarkLib.Scale
local LerpColor = DarkLib.LerpColor
local c = DarkLib.ModifyColor

local Colors = {
    background = DarkUI.Theme.Background,
    backgroundAlpha = c(DarkUI.Theme.Background, 1, 230),
    header = DarkUI.Theme.Primary,
    headerAlpha = c(DarkUI.Theme.Primary, 1, 230)
}

AccessorFunc(PANEL, "m_bIsMenuComponent", "IsMenu", FORCE_BOOL)
AccessorFunc(PANEL, "m_bDraggable", "Draggable", FORCE_BOOL)
AccessorFunc(PANEL, "m_bSizable", "Sizable", FORCE_BOOL)
AccessorFunc(PANEL, "m_bScreenLock", "ScreenLock", FORCE_BOOL)
AccessorFunc(PANEL, "m_bDeleteOnClose", "DeleteOnClose", FORCE_BOOL)
AccessorFunc(PANEL, "m_bPaintShadow", "PaintShadow", FORCE_BOOL)
AccessorFunc(PANEL, "m_iMinWidth", "MinWidth", FORCE_NUMBER)
AccessorFunc(PANEL, "m_iMinHeight", "MinHeight", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bBackgroundBlur", "BackgroundBlur", FORCE_BOOL)
AccessorFunc(PANEL, "m_bCornersRadius", "CornerRadius", FORCE_NUMBER)
AccessorFunc(PANEL, "m_bBigHeader", "BigHeader", FORCE_BOOL)
AccessorFunc(PANEL, "m_bTransparent", "Transparent", FORCE_BOOL)

function PANEL:Init()
    self:SetFocusTopLevel(true)
    --self:SetCursor( "sizeall" )
    -- Buttons
    self.btnClose = vgui.Create("DarkUI_Button", self)
    self.btnClose:SetFont("DarkUI.FrameButton")
    self.btnClose:SetText("✕")
    local offset = ScreenScale(6)
    local offset2 = offset * 2

    self.btnClose.Paint = function(btn, w, h)
        if DarkLib.Materials.Cross and not DarkLib.Materials.Cross:IsError() then
            self.btnClose.TargetColor = btn.Hovered and DarkUI.Theme.Red or color_white
            self.btnClose.CurrentColor = LerpColor(FrameTime() * 10, self.btnClose.CurrentColor or color_white, self.btnClose.TargetColor)
            surface.SetDrawColor(self.btnClose.CurrentColor)
            surface.SetMaterial(DarkLib.Materials.Cross)
            surface.DrawTexturedRect(offset, offset, w - offset2, h - offset2)
        end

        if btn.Loading then
            btn.LoadingColor = btn.LoadingColor or c(DarkUI.Theme.Accent, 1.3)
            DarkLib.DrawLoading(w / 2, h / 2, h - ScreenScale(2), btn.LoadingColor)
        end
    end

    if not DarkLib.Materials.Cross or DarkLib.Materials.Cross:IsError() then
        self.btnClose:SetLoading(true)

        -- Load Cross
        DarkLib.Images.QueueLoadMaterials({
            image = "HgMhjrI",
            func = function(mat)
                DarkLib.Materials.Cross = mat
                if not IsValid(self.btnClose) then return end
                self.btnClose:SetLoading(false)
                self.btnClose:SetText("")
                DarkLib:LogDebug("Imgur Material loaded for close button.")
            end
        })
    else
        self.btnClose:SetText("")
    end

    self.btnClose.DoClick = function(button)
        self:Close()
    end

    -- Title
    self.lblTitle = vgui.Create("DarkUI.Label", self)

    -- TODO: Colors for Window title
    self.lblTitle.UpdateColours = function(label, skin)
        if (self:IsActive()) then return label:SetTextStyleColor(skin.Colours.Window.TitleActive) end

        return label:SetTextStyleColor(skin.Colours.Window.TitleInactive)
    end

    -- Default setup
    self.m_bHeaderHeight = 30
    self:SetDraggable(true)
    self:SetSizable(false)
    self:SetScreenLock(false)
    self:SetDeleteOnClose(true)
    self:SetTitle("Untitled Window")
    self:SetMinWidth(50)
    self:SetMinHeight(50)
    -- This turns off the engine drawing
    self:SetPaintBackgroundEnabled(false)
    self:SetPaintBorderEnabled(false)
    self.m_fCreateTime = SysTime()
    local Padding = ScreenScale(6)
    self:DockPadding(Padding, self.m_bHeaderHeight + Padding, Padding, Padding)
end

function PANEL:ShowCloseButton(bShow)
    self.btnClose:SetVisible(bShow)
end

function PANEL:GetTitle()
    return self.lblTitle:GetText()
end

function PANEL:SetTitle(strTitle)
    self.lblTitle:SetText(strTitle)
end

function PANEL:Close()
    self:SetVisible(false)

    if (self:GetDeleteOnClose()) then
        self:Remove()
    end

    self:OnClose()
end

function PANEL:OnClose()
end

function PANEL:Center()
    self:InvalidateLayout(true)
    self:CenterVertical()
    self:CenterHorizontal()
end

function PANEL:IsActive()
    if (self:HasFocus()) then return true end
    if (vgui.FocusedHasParent(self)) then return true end

    return false
end

function PANEL:SetIcon(str)
    if (not str and IsValid(self.imgIcon)) then return self.imgIcon:Remove() end -- We are instructed to get rid of the icon, do it and bail.

    if (not IsValid(self.imgIcon)) then
        self.imgIcon = vgui.Create("DImage", self)
    end

    if (IsValid(self.imgIcon)) then
        self.imgIcon:SetMaterial(Material(str))
    end
end

function PANEL:SetBackgroundMaterial(material, mode)
    if isstring(material) and type(material) ~= "IMaterial" then
        material = Material(material, "noclamp smooth")
    end

    self._backgroundMaterial = material
    self._backgroundMaterialMode = mode
end

function PANEL:Think()
    local mousex = math.Clamp(gui.MouseX(), 1, ScrW() - 1)
    local mousey = math.Clamp(gui.MouseY(), 1, ScrH() - 1)

    if (self.Dragging) then
        local x = mousex - self.Dragging[1]
        local y = mousey - self.Dragging[2]

        -- Lock to screen bounds if screenlock is enabled
        if (self:GetScreenLock()) then
            x = math.Clamp(x, 0, ScrW() - self:GetWide())
            y = math.Clamp(y, 0, ScrH() - self:GetTall())
        end

        self:SetPos(x, y)
    end

    if (self.Sizing) then
        local x = mousex - self.Sizing[1]
        local y = mousey - self.Sizing[2]
        local px, py = self:GetPos()

        if (x < self.m_iMinWidth) then
            x = self.m_iMinWidth
        elseif (x > ScrW() - px and self:GetScreenLock()) then
            x = ScrW() - px
        end

        if (y < self.m_iMinHeight) then
            y = self.m_iMinHeight
        elseif (y > ScrH() - py and self:GetScreenLock()) then
            y = ScrH() - py
        end

        self:SetSize(x, y)
        self:SetCursor("sizenwse")

        return
    end

    local screenX, screenY = self:LocalToScreen(0, 0)

    if (self.Hovered and self.m_bSizable and mousex > (screenX + self:GetWide() - 20) and mousey > (screenY + self:GetTall() - 20)) then
        self:SetCursor("sizenwse")

        return
    end

    if (self.Hovered and self:GetDraggable() and mousey < (screenY + self.m_bHeaderHeight)) then
        self:SetCursor("sizeall")

        return
    end

    self:SetCursor("arrow")

    -- Don't allow the frame to go higher than 0
    if (self.y < 0) then
        self:SetPos(self.x, 0)
    end
end

function PANEL:Paint(w, h)
    local backgroundColor = self.m_bTransparent and Colors.backgroundAlpha or Colors.background
    local headerColor = self.m_bTransparent and Colors.headerAlpha or Colors.header
    local CornerRadius = ScreenScale(self.m_bCornersRadius or 4)

    if (self.m_bBackgroundBlur) then
        Derma_DrawBackgroundBlur(self, self.m_fCreateTime)
    end

    draw.RoundedBox(CornerRadius, 0, 0, w, h, backgroundColor)
    draw.RoundedBoxEx(CornerRadius, 0, 0, w, self.m_bHeaderHeight, headerColor, CornerRadius, CornerRadius)

    if self._backgroundMaterial then
        local mode = self._backgroundMaterialMode or "stretch"
        surface.SetDrawColor(255, 255, 255, 100)
        surface.SetMaterial(self._backgroundMaterial)

        if mode == "stretch" then
            surface.DrawTexturedRect(0, 20, w, h)
        elseif mode == "center" then
            surface.DrawTexturedRect((w / 2) - (h / 2), 20, h, h - 30)
        end
    end

    return true
end

function PANEL:OnMousePressed()
    local screenX, screenY = self:LocalToScreen(0, 0)

    if (self.m_bSizable and gui.MouseX() > (screenX + self:GetWide() - 20) and gui.MouseY() > (screenY + self:GetTall() - 20)) then
        self.Sizing = {gui.MouseX() - self:GetWide(), gui.MouseY() - self:GetTall()}
        self:MouseCapture(true)

        return
    end

    if (self:GetDraggable() and gui.MouseY() < (screenY + self.m_bHeaderHeight)) then
        self.Dragging = {gui.MouseX() - self.x, gui.MouseY() - self.y}
        self:MouseCapture(true)

        return
    end
end

function PANEL:OnMouseReleased()
    self.Dragging = nil
    self.Sizing = nil
    self:MouseCapture(false)
end

local ScreenScale = DarkLib.Scale
local _padding = ScreenScale(6)

function PANEL:PerformLayout()
    if not self.m_bBigHeader then
        self.m_bHeaderHeight = ScreenScale(30)
        self.lblTitle:SetFont("DarkUI.FrameTitle")
    else
        self.m_bHeaderHeight = ScreenScale(40)
        self.lblTitle:SetFont("DarkUI.DarkUI.FrameTitleBig")
    end

    local HeaderHeight = self.m_bHeaderHeight
    local titlePush = 0

    if (IsValid(self.imgIcon)) then
        self.imgIcon:SetPos(5, HeaderHeight / 2 - (HeaderHeight / 2))
        self.imgIcon:SetSize(HeaderHeight / 2, HeaderHeight / 2)
        titlePush = HeaderHeight / 2
    end

    local WideOffset = HeaderHeight
    self.btnClose:SetPos(self:GetWide() - HeaderHeight, HeaderHeight / 2 - self.btnClose:GetTall() / 2)
    self.btnClose:SetSize(HeaderHeight, HeaderHeight)
    self.lblTitle:SetPos(ScreenScale(8) + titlePush, HeaderHeight / 2 - (self.lblTitle:GetTall() / 2))
    self.lblTitle:SetSize(self:GetWide() - HeaderHeight - titlePush, HeaderHeight)
    self:DockPadding(_padding, HeaderHeight + _padding, _padding, _padding)
end

derma.DefineControl("DarkUI_Frame", "DarkUI Window", PANEL, "EditablePanel")