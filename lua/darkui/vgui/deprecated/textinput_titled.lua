--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local Scale = DarkLib.Scale
local PANEL = {}

function PANEL:Init()
    -- 70 if both are enabled, at least 30 for only the input, 48 with title
    self:SetTall(Scale(48))
    self:DockPadding(0, 0, 0, 0)
    self.textentry = vgui.Create("DarkUI.Input", self)
    self.textentry:Dock(FILL)
    DarkLib:LogWarn("Usage of testinput_titled may be uncool, i recommend to make your own 'advanced' text input as this one may not work correctly and may be removed.")
end

-- Label functions
function PANEL:SetupTitle()
    -- Setup Title in case it doesnt exists
    if not IsValid(self.label) then
        self.label = vgui.Create("DarkUI.Label", self)
        self.label:Dock(TOP)
        self:InvalidateLayout(true)
    end

    return self.label
end

function PANEL:SetTitle(str)
    self:SetupTitle():SetText(str)
end

function PANEL:SetTitleFont(font)
    local label = self:SetupTitle()
    label:SetFont(font)
    label:SizeToContents()
    self:InvalidateLayout(true)
end

function PANEL:SetTitleColor(color)
    local label = self:SetupTitle()
    label:SetTextColor(color)
end

function PANEL:GetLabel()
    return self:SetupTitle()
end

-- Message functions
function PANEL:SetupMessage()
    -- Setup Message in case it doesnt exists
    if not IsValid(self.message) then
        self.message = vgui.Create("DarkUI.Label", self)
        self.message:Dock(BOTTOM)
        self.message:SetFont("DarkUI.TextSmall")
        self:InvalidateLayout(true)
    end

    return self.message
end

function PANEL:SetMessage(str)
    self:SetupMessage():SetText(str)
end

function PANEL:SetMessageFont(font)
    local message = self:SetupMessage()
    message:SetFont(font)
    message:SizeToContents()
    self:InvalidateLayout(true)
end

function PANEL:SetMessageColor(color)
    local message = self:SetupMessage()
    message:SetTextColor(color)
end

function PANEL:GetMessage()
    return self:SetupMessage()
end

-- TextEntry functions
function PANEL:SetText(str)
    self.textentry:SetText(str)
end

function PANEL:GetText()
    return self.textentry:GetText()
end

-- Translate basic stuff
do
    function PANEL:SetIcon(icon, left)
        self.textentry:SetIcon(icon, left)
    end

    function PANEL:SetImgurIcon(icon, left)
        self.textentry:SetImgurIcon(icon, left)
    end

    function PANEL:GetIcon()
        return self.textentry:GetIcon()
    end

    function PANEL:SetPlaceholder(string)
        self.textentry:SetPlaceholder(string)
    end

    function PANEL:SetMultiLine(multiline, scrollbar)
        self.textentry:SetMultiLine(multiline, scrollbar)
    end

    function PANEL:SetOnEnter(cllbck)
        self.textentry:SetOnEnter(cllbck)

        return self
    end

    function PANEL:SetOnChange(cllbck)
        self.textentry:SetOnChange(cllbck)

        return self
    end

    function PANEL:SetCornerRadius(int)
        self.textentry:SetCornerRadius(int)
    end

    function PANEL:SetBackgroundColor(color)
        self.textentry:SetBackgroundColor(color)
    end

    function PANEL:SetTextColor(color)
        self.textentry:SetTextColor(color)
    end

    function PANEL:SetPlaceholderColor(color)
        self.textentry:SetPlaceholderColor(color)
    end

    function PANEL:SetTranslatePlaceholder(bool)
        self.textentry:SetTranslatePlaceholder(bool)
    end

    function PANEL:SetUpdateOnType(bool)
        self.textentry:SetUpdateOnType(bool)
    end

    function PANEL:SetNumeric(bool)
        self.textentry:SetNumeric(bool)
    end
end

function PANEL:GetTextEntry()
    return self.textentry
end

-- Required stuff
function PANEL:OnMousePressed()
    self.textentry:RequestFocus()
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local ctrl = vgui.Create(ClassName)
    ctrl:SetTitle("Some Text Input with label.......")
    ctrl:SetMessage("Some Message")
    --ctrl:SetTheme("BmanGames")
    ctrl:SetText("Edit Me!")
    ctrl:SetPlaceholder("#test")
    --ctrl:SetMultiLine(true, true)
    ctrl:SetWide(200)
    ctrl:SetTall(70) -- Recommend minimum size for both lol
    ctrl:SetTranslatePlaceholder(true)
    --ctrl.OnEnter = function(self)
    --    Derma_Message("You Typed: " .. self:GetValue())
    --end
    ctrl:SetImgurIcon("HgMhjrI")
    ctrl:SetUpdateOnType(true)
    PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
end

derma.DefineControl("DarkUI.InputAdvanced", "A Advanced Input", PANEL, "DarkUI_Panel")