--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local PANEL = {}
AccessorFunc(PANEL, "m_bAutoStretchVertical", "AutoStretchVertical", FORCE_BOOL)

function PANEL:Init()
    -- Default
    self.Theme = DarkUI.GetTheme() -- Set the Theme
end

function PANEL:MirrorChild(name, element)
    self[name] = function(pnl, ...) return element[name](element, ...) end
end

function PANEL:OverwriteChildFunc(name, element)
    self[name] = function(self, cllbck)
        element[name] = function(_, ...) return cllbck(self, ...) end

        return self
    end

    self["Call" .. name] = function(self) return element[name](self) end
end

function PANEL:Paint(w, h)
    return false
end

function PANEL:SetTheme(name)
    self.Theme = DarkUI.GetTheme(name or "Default")

    return self
end

function PANEL:GetContentSize()
    return self:ChildrenSize()
end

function PANEL:Think()
    if (self:GetAutoStretchVertical()) then
        self:SizeToContentsY()
    end
end

derma.DefineControl("DarkUI_Panel", "A simple panel", PANEL, "DPanel")