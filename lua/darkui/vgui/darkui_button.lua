--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local PANEL = {}
AccessorFunc(PANEL, "m_oldtext", "OldText")
AccessorFunc(PANEL, "m_largebutton", "Large", FORCE_BOOL)
local ScreenScale = DarkLib.Scale
local c = DarkLib.ModifyColor

function PANEL:Init()
    -- self.Colors 
    self.THEME = DarkUI.GetTheme()

    self.Colors = {
        -- Background self.Colors
        Default = c(self.THEME.Accent, 0.9),
        Hover = c(self.THEME.Accent, 0.8),
        Pressed = c(self.THEME.Accent, 0.7),
        Disabled = c(self.THEME.Accent, 0.5),
        Invalid = self.THEME.Red,
        -- Text self.Colors
        DefaultText = self.THEME.Text.Default,
        HoverText = self.THEME.Text.Brighter,
        PressedText = self.THEME.Text.Darker,
        DisabledText = self.THEME.Text.Default,
        InvalidText = self.THEME.Text.Default
    }

    -- Default
    self:SetFont("DarkUI.Text")
    self:InvalidateLayout(true)
    self:SetTall(ScreenScale(22))
    self:InvalidateLayout(true)
    self.Material = nil
    self.LoadingColor = c(self.THEME.Accent, 1.3)
end

function PANEL:UpdateCursor()
    if self:GetDisabled() or self.Loading or not self:checkValid() then
        self:SetCursor("no")
    else
        self:SetCursor("hand")
    end
end

function PANEL:OnCursorEntered()
    self:UpdateCursor()
end

function PANEL:Think()
    if self.LoadingTimeout and self.LoadingTimeout < CurTime() then
        self:SetLoading(false, false)
    end
end

function PANEL:SetLoading(bool, time)
    self.Loading = bool
    self.LoadingTimeout = (time and CurTime() + time) or false

    if self.Loading then
        if self.m_oldtext ~= nil then return end
        self.m_oldtext = self:GetText()
        self:SetText("")
    else
        if self.m_oldtext == nil then return end
        self:SetText(self.m_oldtext or "")
        self.m_oldtext = nil
    end

    self:UpdateCursor()
end

function PANEL:Paint(w, h)
    local DisabledColor = self._disabledColor or self.Colors.Disabled

    if self:GetDisabled() then
        draw.RoundedBox(self.m_bCornersRadius or 0, 0, 0, w, h, DisabledColor)
    else
        local InvalidColor = self._invalidColor or self.Colors.Invalid
        local DefaultColor = self._defaultColor or self.Colors.Default
        local HoverColor = self._hoverColor or self.Colors.Hover
        local PressedColor = self._pressedColor or self.Colors.Pressed
        --draw.RoundedBox(self.m_bCornersRadius, 0, 0, w, h, self:checkValid() and ((self:IsHovered() and not self.Depressed) and HoverColor or DefaultColor) or InvalidColor)
        draw.RoundedBox(self.m_bCornersRadius or 0, 0, 0, w, h, (not self:checkValid() and InvalidColor) or ((self.Loading or self.Depressed) and PressedColor) or (self:IsHovered() and HoverColor) or DefaultColor)
    end

    if self.Loading then
        DarkLib.DrawLoading(w / 2, h / 2, h - (h * 0.3), self.LoadingColor)
    end
end

function PANEL:UpdateColours(skin)
    if (not self:IsEnabled()) then return self:SetTextStyleColor(self._disabledTextColor or self.Colors.DisabledText) end
    if (not self:checkValid()) then return self:SetTextStyleColor(self._invalidTextColor or self.Colors.InvalidText) end
    if (self:IsDown() or self.m_bSelected) then return self:SetTextStyleColor(self._pressedTextColor or self.Colors.PressedText) end
    if (self.Hovered) then return self:SetTextStyleColor(self._hoverTextColor or self.Colors.HoverText) end

    return self:SetTextStyleColor(self._defaultTextColor or self.Colors.DefaultText)
end

function PANEL:checkValid()
    return true
end

function PANEL:SetTheme(theme)
    self._skins = self._skins or {}
    local skin = self._skins[theme or "default"]
    if not skin then return end
    self:SetDefaultColor(skin.default, skin.defaultText)
    self:SetHoverColor(skin.hover, skin.hoverText)
    self:SetDisabledColor(skin.disabled, skin.disabledText)
    self:SetInvalidColor(skin.invalid, skin.invalidText)
    self:SetPressedColor(skin.pressed, skin.pressedText)
    self:UpdateColours()

    return self
end

function PANEL:CreateTheme(name, skin)
    self._skins = self._skins or {}
    self._skins[name] = skin

    if name == "default" then
        self:SetTheme("default")
    end
end

-- Default Colors
PANEL._defaultColor = nil
PANEL._defaultTextColor = nil
-- Colors when pressed
PANEL._pressedColor = nil
PANEL._pressedTextColor = nil
-- Colors when hovered
PANEL._hoverColor = nil
PANEL._hoverTextColor = nil
-- Colors when invalid
PANEL._invalidColor = nil
PANEL._invalidTextColor = nil
-- Colors when disabled
PANEL._disabledColor = nil
PANEL._disabledTextColor = nil

-- Setters
function PANEL:SetDefaultColor(color, textColor)
    self._defaultColor = IsColor(color) and color or self.Colors.Default
    self._defaultTextColor = IsColor(textColor) and textColor or self.Colors.DefaultText
end

function PANEL:SetHoverColor(color, textColor)
    self._hoverColor = IsColor(color) and color or self.Colors.Hover
    self._hoverTextColor = IsColor(textColor) and textColor or self.Colors.HoverText
end

function PANEL:SetDisabledColor(color, textColor)
    self._disabledColor = IsColor(color) and color or self.Colors.Disabled
    self._disabledTextColor = IsColor(textColor) and textColor or self.Colors.DisabledText
end

function PANEL:SetInvalidColor(color, textColor)
    self._invalidColor = IsColor(color) and color or self.Colors.Invalid
    self._invalidTextColor = IsColor(textColor) and textColor or self.Colors.InvalidText
end

function PANEL:SetPressedColor(color, textColor)
    self._pressedColor = IsColor(color) and color or self.Colors.Pressed
    self._pressedTextColor = IsColor(textColor) and textColor or self.Colors.PressedText
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local ctrl = vgui.Create(ClassName)
    ctrl:SetText("Example Button")
    ctrl:SetWide(200)
    PropertySheet:AddSheet(ClassName, ctrl, nil, true, true)
end

function PANEL:PerformLayout()
    local SmallSize, LargeSize = ScreenScale(22), ScreenScale(33)

    if (IsValid(self.m_Image)) then
        self.m_Image:SetPos(ScreenScale(4), (self:GetTall() - self.m_Image:GetTall()) * 0.5)
        self:SetTextInset(self.m_Image:GetWide() + ScreenScale(16), 0)
    end

    local _, h = self:GetSize()

    if self.m_largebutton then
        if h == SmallSize then
            self:SetHeight(LargeSize)
            self:SetFont("DarkUI.TextLarge")
        end
    else
        if h == LargeSize then
            self:SetHeight(SmallSize)
            self:SetFont("DarkUI.Text")
        end
    end

    DLabel.PerformLayout(self)
end

derma.DefineControl("DarkUI_Button", "DarkUI Button", PANEL, "DButton")