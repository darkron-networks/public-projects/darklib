--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local PANEL = {}
AccessorFunc(PANEL, "Padding", "Padding")
AccessorFunc(PANEL, "pnlCanvas", "Canvas")

function PANEL:Init()
    self.pnlCanvas = vgui.Create("DarkUI_Panel", self)
    self.pnlCanvas.CurrentOffset = 0
    self.pnlCanvas.TargetOffset = 0
    self.m_bSmoothed = true

    --self.m_bSmoothFactor = 0.15
    function self.pnlCanvas.Think(cns)
        if self.m_bSmoothed and cns.CurrentOffset ~= cns.TargetOffset then
            cns.CurrentOffset = Lerp(self.m_bSmoothFactor or (FrameTime() * 10), cns.CurrentOffset, cns.TargetOffset)
            cns:SetPos(0, cns.CurrentOffset)
        end
    end

    self.pnlCanvas.OnMousePressed = function(self, code)
        self:GetParent():OnMousePressed(code)
    end

    self.pnlCanvas:SetMouseInputEnabled(true)

    self.pnlCanvas.PerformLayout = function(pnl)
        self:PerformLayout()
        self:InvalidateParent()
    end

    -- Create the scroll bar
    self.VBar = vgui.Create("DarkUI_ScrollBar", self)
    self.VBar:Dock(RIGHT)
    self:SetPadding(0)
    self:SetMouseInputEnabled(true)
    -- This turns off the engine drawing
    self:SetPaintBackgroundEnabled(false)
    self:SetPaintBorderEnabled(false)
    self:SetPaintBackground(false)
end

-- Can we put it in the scrollbar itself?
function PANEL:Think()
    local vbar = self.VBar
    local canvasHeight = self.pnlCanvas:GetTall()
    YPos = vbar:GetOffset()
    local totalOffset = -1 * YPos + vbar:GetTall()

    if totalOffset >= canvasHeight then
        vbar:SetScroll(canvasHeight)
    end
end

function PANEL:AddItem(pnl)
    pnl:SetParent(self:GetCanvas())
end

function PANEL:OnChildAdded(child)
    self:AddItem(child)
end

function PANEL:SizeToContents()
    self:SetSize(self.pnlCanvas:GetSize())
end

function PANEL:GetVBar()
    return self.VBar
end

function PANEL:GetCanvas()
    return self.pnlCanvas
end

function PANEL:InnerWidth()
    return self:GetCanvas():GetWide()
end

function PANEL:Rebuild()
    self:GetCanvas():SizeToChildren(false, true)

    -- Although this behaviour isn't exactly implied, center vertically too
    if (self.m_bNoSizing and self:GetCanvas():GetTall() < self:GetTall()) then
        self:GetCanvas():SetPos(0, (self:GetTall() - self:GetCanvas():GetTall()) * 0.5)
    end
end

function PANEL:OnMouseWheeled(dlta)
    return self.VBar:OnMouseWheeled(dlta)
end

function PANEL:OnVScroll(iOffset)
    if self.m_bSmoothed then
        self.pnlCanvas.TargetOffset = iOffset
    else
        self.pnlCanvas:SetPos(0, iOffset)
    end
end

function PANEL:ScrollToChild(panel)
    self:PerformLayout()
    local x, y = self.pnlCanvas:GetChildPosition(panel)
    local w, h = panel:GetSize()
    y = y + h * 0.5
    y = y - self:GetTall() * 0.5
    self.VBar:AnimateTo(y, 0.5, 0, 0.5)
end

function PANEL:PerformLayout()
    local Tall = self.pnlCanvas:GetTall()
    local Wide = self:GetWide()
    local YPos = 0
    self:Rebuild()
    self.VBar:SetUp(self:GetTall(), self.pnlCanvas:GetTall())
    YPos = self.VBar:GetOffset()

    if (self.VBar.Enabled) then
        Wide = Wide - self.VBar:GetWide()
    end

    self.pnlCanvas:SetPos(0, YPos)
    self.pnlCanvas:SetWide(Wide)
    self:Rebuild()

    if (Tall ~= self.pnlCanvas:GetTall()) then
        self.VBar:SetScroll(self.VBar:GetScroll()) -- Make sure we are not too far down!
    end
end

function PANEL:Clear()
    return self.pnlCanvas:Clear()
end

function PANEL:SetAnimationTime(time)
    assert(isnumber(time), "SetAnimationTime only accepts a Number value.")
    self.m_bSmoothFactor = time
    self.VBar:SetAnimationTime(time)
end

function PANEL:SetSmooth(bool)
    assert(isbool(bool), "SetSmooth only accepts a Bool value.")
    self.m_bSmoothed = bool
    self.VBar:SetSmooth(bool)
end

function PANEL:GenerateExample(ClassName, PropertySheet, Width, Height)
    local panel = vgui.Create("DarkUI_Panel")
    panel:SetSize(300, 200)
    panel:DockPadding(4, 4, 4, 4)
    panel._Color = Color(0, 0, 0, 20)

    panel.Paint = function(self, w, h)
        draw.RoundedBox(2, 0, 0, w, h, self._Color)
    end

    local ctrl = vgui.Create(ClassName, panel)
    ctrl:Dock(FILL)

    for i = 1, 128 do
        local label = vgui.Create("DLabel", ctrl)
        label:SetText("Label: " .. i)
        label:Dock(TOP)
    end

    PropertySheet:AddSheet(ClassName, panel, nil, true, true)
end

derma.DefineControl("DarkUI_ScrollPanel", "DarkUI Scrollpanel", PANEL, "DPanel")