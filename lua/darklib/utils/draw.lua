--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
-- Preparing stuff 
local arcCache = {}
local vertCache = {} -- Thanks to Zak for this idea
local CurTime = CurTime
local Arc = DarkLib.Arc
local math_rad = math.rad
local math_cos = math.cos
local math_sin = math.sin
local math_ceil = math.ceil
local textureLoading = false
local arcTexture = Material("error")
local surface_SetFont, surface_GetTextSize = surface.SetFont, surface.GetTextSize
local draw_RoundedBox, draw_RoundedBoxEx = draw.RoundedBox, draw.RoundedBoxEx
local draw_SimpleText = draw.SimpleText
local cam_Start3D2D, cam_End3D2D = cam.Start3D2D, cam.End3D2D
local surface_SetMaterial = surface.SetMaterial
local surface_SetDrawColor = surface.SetDrawColor
local surface_DrawTexturedRectRotated = surface.DrawTexturedRectRotated
local surface_DrawPoly = surface.DrawPoly
local surface_DrawRect, math_Round = surface.DrawRect, math.Round

for i = 1, 256 do
    vertCache[i] = {
        x = 0,
        y = 0,
        u = 0,
        v = 0
    }
end

-- Draws a triangle
function DarkLib.DrawTriangle(centerX, centerY, width, height, angle, rotate_uv)
    local v1, v2, v3, vn = vertCache[1], vertCache[2], vertCache[3], vertCache[4]
    v1.x = centerX - (width / 2) -- Breite
    v1.y = centerY + (height / 2) -- Höhe
    --
    v2.x = centerX
    v2.y = centerY - (height / 2)
    --
    v3.x = centerX + (width / 2)
    v3.y = centerY + (height / 2)
    vertCache[4] = nil

    -- Rotate it in case there is an angle over 0
    if angle and angle ~= 0 then
        rotation = math_rad(angle)
        local c = math_cos(rotation)
        local s = math_sin(rotation)

        for i = 1, #vertCache do
            local vertex = vertCache[i]

            if vertex == nil then
                -- Ignore any other vertex in the cache
                goto triangle_draw
            end

            local vx, vy = vertex.x, vertex.y
            vx = vx - centerX
            vy = vy - centerY
            vertex.x = centerX + (vx * c - vy * s)
            vertex.y = centerY + (vx * s + vy * c)

            if not rotate_uv then
                local u, v = vertex.u, vertex.v
                u, v = u - 0.5, v - 0.5
                vertex.u = 0.5 + (u * c - v * s)
                vertex.v = 0.5 + (u * s + v * c)
            end
        end
    end

    ::triangle_draw::
    surface_DrawPoly(vertCache)
    vertCache[4] = vn
end

-- Draws a hollow Box (also known as "border")
function DarkLib.DrawOutlinedBox(x, y, w, h, thickness, col)
    thickness = math_Round(thickness or 1)
    surface_SetDrawColor(col.r, col.g, col.b, col.a)
    surface_DrawRect(x, y + thickness, thickness, h - thickness * 2) -- Left
    surface_DrawRect(x + w - thickness, y + thickness, thickness, h - thickness * 2) -- Right
    surface_DrawRect(x, y, w, thickness) -- Top
    surface_DrawRect(x, y + h - thickness, w, thickness) -- Bottom
end

local _DrawOutlinedBox = DarkLib.DrawOutlinedBox

function DarkLib.DrawOutlinedBoxRounded(corners, x, y, w, h, thickness, col)
    thickness = thickness or 1
    corners = corners and math_ceil(corners) or 0
    -- When corners are less than 1 pixel it will be like a normal box, no need to draw arcs here
    if corners < 1 then return _DrawOutlinedBox(x, y, w, h, thickness, col) end

    if col then
        surface.SetDrawColor(col)
    end

    -- Draw cornerless rectangle 
    surface_DrawRect(x, y + corners, thickness, h - corners * 2) -- Left
    surface_DrawRect(x + w - thickness, y + corners, thickness, h - corners * 2) -- Right
    surface_DrawRect(x + corners, y, w - corners * 2, thickness) -- Top
    surface_DrawRect(x + corners, y + h - thickness, w - corners * 2, thickness) -- Bottom

    -- Download material if required (1x1 pixel material)
    if arcTexture:IsError() and not textureLoading then
        textureLoading = true

        DarkLib.Images.QueueLoadMaterials({
            image = "Kr19qDP",
            func = function(mat)
                arcTexture = mat
                textureLoading = nil
            end
        })
    end

    -- Set draw material to something white we can paint on
    surface.SetMaterial(arcTexture)
    -- Draw Arcs
    local cacheid = corners .. x .. y .. w .. h .. thickness
    local cachedArcs = arcCache[cacheid]

    if not cachedArcs then
        cachedArcs = {
            [1] = Arc.Precache(x + corners, y + corners, corners, thickness, 90, 180, 45), -- Top Left
            [2] = Arc.Precache(x + w - corners, y + corners, corners, thickness, 0, 90, 45), -- Top Right
            [3] = Arc.Precache(x + corners, y + h - corners, corners, thickness, 180, 270, 45), -- Bottom Left
            [4] = Arc.Precache(x + w - corners, y + h - corners, corners, thickness, 270, 360, 45) -- Bottom Right
        }

        arcCache[cacheid] = cachedArcs
    end

    Arc.DrawArc(cachedArcs[1])
    Arc.DrawArc(cachedArcs[2])
    Arc.DrawArc(cachedArcs[3])
    Arc.DrawArc(cachedArcs[4])
end

-- Draws a Loading image (which ROTATES!)
local _isLoading = false

function DarkLib.DrawLoading(x, y, size, color)
    if DarkLib.Materials.Loading:IsError() and not _isLoading then
        -- Load Loading image on startup
        _isLoading = true

        DarkLib.Images.QueueLoadMaterials({
            image = "mJglYYR",
            func = function(mat)
                DarkLib.Materials.Loading = mat
            end
        })
    end

    surface_SetMaterial(DarkLib.Materials.Loading)
    surface_SetDrawColor(color)
    surface_DrawTexturedRectRotated(x, y, size, size, (CurTime() % 360) * -100)
end

-- Draws an overhead text over entities
function DarkLib.Overhead(entity, text, data)
    if not IsValid(entity) then return end
    local data = data or {}
    local THEME = data.THEME or DarkUI.GetTheme()
    local font = data.font or "DarkUI.OverheadText"
    local scale = data.scale or 0.035
    local drawFunc = data.drawFunc or false
    local text = text or "Invalid"
    local _, maxs = entity:GetModelBounds()
    local Pos = entity:GetPos() + Vector(0, 0, maxs.z + (offset or 5))
    local Ang = Angle(0, LocalPlayer():EyeAngles().y - 90, 90)
    -- Overhead
    cam_Start3D2D(Pos, Ang, scale)

    if drawFunc then
        drawFunc(text, font, data)
    else
        surface_SetFont(font)
        local p = 16
        local w, h = surface_GetTextSize(text)
        w = w + p * 2.3
        h = h + p * 1.5
        draw_RoundedBox(6, -w * 0.5, -p, w, h, THEME.Background)

        if data.DrawLineTop then
            draw_RoundedBoxEx(6, -w * 0.5, -p, w, 16, THEME.Accent, true, true, false, false)
        else
            draw_RoundedBoxEx(6, -w * 0.5, h - 20, w, 16, THEME.Accent, false, false, true, true)
        end

        draw_SimpleText(text, font, 0, 0, THEME.Text.Default, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
    end

    cam_End3D2D()
end