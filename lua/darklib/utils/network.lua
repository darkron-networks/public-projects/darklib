--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0

    The networking is still very WIP
]]
local Network = {}

if SERVER then
    Network.Add = util.AddNetworkString -- Copy of AddNetworkString in case i want to add stuff later
end

-- Type Enums
local dTypes = {
    BIT = 0,
    INT = 1,
    UINT = 2,
    FLOAT = 3,
    DOUBLE = 4,
    STRING = 5
}

--[[
    Sender
]]
local Sender = {}

do
    Sender.__index = Sender
    Sender.__tostring = function(self) return string.format("PacketSender[%s] %d Bits", self.id, self:GetBits()) end

    -- utils
    function Sender:GetBits()
        return self.bits
    end

    function Sender:GetBytes()
        return math.ceil(self.bits / 8)
    end

    function Sender:GetKB()
        return math.floor(self:GetBytes() / 1024)
    end

    -------------- Core Networking
    -- Writes a bit
    function Sender:WriteBit(b)
        self.data[#self.data + 1] = {
            typ = dTypes.BIT,
            val = b
        }

        self.bits = self.bits + 1

        return self
    end

    Sender.WriteBool = Sender.WriteBit

    -- Writes an Int (number Number, number Bits)
    function Sender:WriteInt(n, b)
        self.data[#self.data + 1] = {
            typ = dTypes.INT,
            val = n,
            bits = b
        }

        self.bits = self.bits + b

        return self
    end

    -- Writes an UInt (number Number, number Bits)
    function Sender:WriteUInt(n, b)
        self.data[#self.data + 1] = {
            typ = dTypes.UINT,
            val = n,
            bits = b
        }

        self.bits = self.bits + b

        return self
    end

    -- Writes an Float
    function Sender:WriteFloat(f)
        self.data[#self.data + 1] = {
            typ = dTypes.FLOAT,
            val = f
        }

        self.bits = self.bits + 32

        return self
    end

    -- Writes an Double (larger Float but with more data)
    function Sender:WriteDouble(d)
        self.data[#self.data + 1] = {
            typ = dTypes.DOUBLE,
            val = d
        }

        self.bits = self.bits + 64

        return self
    end

    -- Writes an String
    function Sender:WriteString(s)
        self.data[#self.data + 1] = {
            typ = dTypes.STRING,
            val = s
        }

        self.bits = self.bits + (s:len() + 1) * 8

        return self
    end

    -------------- Extra Networking
    -------------- They depend on the Core and only add stuff for useability
    function Sender:WriteVector(v)
        self:WriteFloat(v.x)
        self:WriteFloat(v.y)
        self:WriteFloat(v.z)

        return self
    end

    Sender.WriteNormal = Sender.WriteVector

    function Sender:WriteColor(c)
        self:WriteUInt(c.r, 8)
        self:WriteUInt(c.g, 8)
        self:WriteUInt(c.b, 8)

        if c.a ~= 255 then
            self:WriteBool(true)
            self:WriteUInt(c.a, 8)
        else
            self:WriteBool(false)
        end

        return self
    end

    function Sender:WriteData(d, l)
        self:WriteUInt(l, 16)

        for i = 1, l do
            self:WriteUInt(string.byte(string.sub(d, i, i)), 8)
        end

        return self
    end

    function Sender:WriteTable(t)
        DarkLib:LogDebug("[NET] Sending/Receiving tables should be avoided!")
        self.mp = self.mp or DarkLib.messagePack.new()
        local data = util.Compress(self.mp.pack(t))
        self:WriteData(data, #data)

        return self
    end

    -- Setup and Send the Package (When no player is given it will broadcast it, RecipientFilter also works)
    function Sender:Send(ply)
        if DarkLib.Debug:GetBool() then
            local receiver = "SERVER"

            if SERVER then
                local t = type(ply)

                if not ply then
                    receiver = "everyone"
                elseif t == "table" then
                    receiver = ""

                    for k, _ply in ipairs(ply) do
                        receiver = receiver .. (k == 1 and "" or ", ") .. tostring(_ply)
                    end
                else
                    receiver = tostring(ply)
                end
            end

            DarkLib:LogDebug("[NET] Sending Packet " .. self.id .. " (" .. self:GetBytes() .. " Bytes) to " .. receiver)
        end

        if self:GetKB() > 64 then
            DarkLib:LogError("[NET] Packet " .. self.id .. " too big! (" .. self:GetKB() .. "/64 KB)")

            return false
        end

        net.Start(self.id)

        for i = 1, #self.data do
            local d = self.data[i]

            if d.typ == dTypes.BIT then
                net.WriteBit(isbool(d.val) and d.val or d.val == 1)
            end

            if d.typ == dTypes.INT then
                net.WriteInt(d.val, d.bits)
            end

            if d.typ == dTypes.UINT then
                net.WriteUInt(d.val, d.bits)
            end

            if d.typ == dTypes.FLOAT then
                net.WriteFloat(d.val)
            end

            if d.typ == dTypes.DOUBLE then
                net.WriteDouble(d.val)
            end

            if d.typ == dTypes.STRING then
                net.WriteString(d.val)
            end
        end

        if SERVER then
            return ply and net.Send(ply) or net.Broadcast()
        else
            net.SendToServer()
        end
    end
end

function Network.Sender(id)
    return setmetatable({
        id = id,
        data = {},
        bits = 0
    }, Sender)
end

--[[
    Receiver
]]
local Receiver = {}

do
    Receiver.__index = Receiver
    Receiver.__tostring = function(self) return string.format("PacketReceiver[%s] %d Bits", self.id, self:GetBits()) end

    -- utils
    function Receiver:GetBits()
        return self.bits
    end

    function Receiver:GetBitsLeft()
        return self.bits_left
    end

    function Receiver:HasDataLeft()
        return self.bits_left > 0
    end

    function Receiver:GetBytes()
        return math.ceil(self.bits / 8)
    end

    function Receiver:GetKB()
        return math.floor(self:GetBytes() / 1024)
    end

    function Receiver:GetPlayer()
        return self.ply
    end

    -- Network
    function Receiver:ReadBit()
        self.bits_left = self.bits_left - 1

        return net.ReadBit()
    end

    function Receiver:ReadInt(b)
        self.bits_left = self.bits_left - b

        return net.ReadInt(b)
    end

    function Receiver:ReadUInt(b)
        self.bits_left = self.bits_left - b

        return net.ReadUInt(b)
    end

    function Receiver:ReadFloat()
        self.bits_left = self.bits_left - 32

        return net.ReadFloat()
    end

    function Receiver:ReadDouble()
        self.bits_left = self.bits_left - 64

        return net.ReadDouble()
    end

    function Receiver:ReadString()
        local s = net.ReadString()
        self.bits_left = self.bits_left - (s:len() + 1) * 8

        return s
    end

    function Receiver:ReadBool()
        self.bits_left = self.bits_left - 1

        return net.ReadBool()
    end

    --------------  Extra Networking
    function Receiver:ReadVector()
        return Vector(self:ReadFloat(), self:ReadFloat(), self:ReadFloat())
    end

    Receiver.ReadNormal = Receiver.ReadVector

    function Receiver:ReadColor()
        return Color(self:ReadUInt(8), self:ReadUInt(8), self:ReadUInt(8), self:ReadBool() and self:ReadUInt(8) or 255)
    end

    function Receiver:ReadData()
        local l = self:ReadUInt(16)
        local d = ""

        for i = 1, l do
            d = d .. string.char(self:ReadUInt(8))
        end

        return d
    end

    function Receiver:ReadTable()
        DarkLib:LogDebug("[NET] Sending/Receiving tables should be avoided!")

        if SERVER then
            ErrorNoHalt("Do NOT allow sending tables from client to server!")
        end

        self.mp = self.mp or DarkLib.messagePack.new()

        return self.mp.unpack(util.Decompress(self:ReadData()))
    end

    -- Fast way for response messages
    function Receiver:Response()
        return Network.Sender(self.id)
    end
end

function Network.Receiver(id, callback, cooldown)
    if not (id and isfunction(callback)) then return end

    if cooldown ~= nil then
        assert(isnumber(cooldown), "Cooldown needs to be an number!")
    end

    net.Receive(id, function(len, ply)
        -- We check the cooldown here, no need to have it in the receiver
        if cooldown and cooldown > 0 then
            ply.__darklib = ply.__darklib or {}
            ply.__darklib.netcooldown = ply.__darklib.netcooldown or {}

            if (ply.__darklib.netcooldown["receiver_" .. id] or 0) > RealTime() then
                DarkLib:LogDebug("[NET] [COOLDOWN] Dropped Packet[" .. id .. "]  (" .. len .. " Bits) from " .. (IsValid(ply) and (tostring(ply) .. " (" .. ply:SteamID() .. ")") or "SERVER"))

                return
            end

            ply.__darklib.netcooldown["receiver_" .. id] = RealTime() + cooldown
        end

        -- Only do debug stuff when debug is enabled
        if DarkLib.Debug:GetBool() then
            DarkLib:LogDebug("[NET] Received Packet[" .. id .. "] (" .. len .. " Bits) from " .. (IsValid(ply) and (tostring(ply) .. " (" .. ply:SteamID() .. ")") or "SERVER"))
        end

        -- Only accept it when player is valid (in case they left while it was received)
        if SERVER and not IsValid(ply) then
            DarkLib:LogDebug("[NET] Received Packet[" .. id .. "] (" .. len .. " Bits) but player was invalid.")

            return
        end

        callback(setmetatable({
            id = id,
            bits = len,
            bits_left = len,
            ply = ply or LocalPlayer()
        }, Receiver))
    end)
end

DarkLib.Net = Network