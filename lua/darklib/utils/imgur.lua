--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local fetch = http.Fetch
DarkLib.Images = {}
DarkLib.Images._ImgurCache = {}
file.CreateDir("darklib/images")

function DarkLib.Images.LoadMaterial(imgur_id, callback, proxy)
    if DarkLib.Images._ImgurCache[imgur_id] ~= nil then
        -- Already in cache
        DarkLib:LogDebug("Already Cached #" .. imgur_id)
        callback(DarkLib.Images._ImgurCache[imgur_id])
    else
        DarkLib:LogDebug("Loading Imgur image from file Cache #" .. imgur_id)

        -- Not in cache
        if file.Exists("darklib/images/" .. imgur_id .. ".png", "DATA") then
            -- File in data cache
            DarkLib:LogDebug("Loading Imgur image from file Cache #" .. imgur_id)
            DarkLib.Images._ImgurCache[imgur_id] = Material("data/darklib/images/" .. imgur_id .. ".png", "noclamp smooth")
            callback(DarkLib.Images._ImgurCache[imgur_id])
        else
            DarkLib:LogDebug("Loading Imgur image from website #" .. imgur_id)

            -- File not in data cache
            fetch((proxy and "https://proxy.duckduckgo.com/iu/?u=https://i.imgur.com/" or "https://i.imgur.com/") .. imgur_id .. ".png", function(body, size)
                -- Fetched stuff
                if not body or tonumber(size) == 0 then
                    callback(nil)

                    return
                end

                file.Write("darklib/images/" .. imgur_id .. ".png", body)
                DarkLib.Images._ImgurCache[imgur_id] = Material("data/darklib/images/" .. imgur_id .. ".png", "noclamp smooth")
                callback(DarkLib.Images._ImgurCache[imgur_id])
            end, function(error)
                -- Got error
                if not proxy then
                    -- Try proxy if there was an error
                    DarkLib.Images.LoadMaterial(imgur_id, callback, proxy)

                    return
                end

                DarkLib:LogError("Error loading Imgur image #" .. imgur_id)
                callback(nil)
            end)
        end
    end
end

function DarkLib.Images.LoadMaterialAsync(imgur_id, proxy)
    -- Creating new promise
    local p = DarkLib.Promise.new()

    DarkLib.Images.LoadMaterial(imgur_id, function(result)
        if result and not result:IsError() then
            -- Success!
            p:resolve(result)
        else
            -- Error!
            p:reject(error)
        end
    end, proxy)
    -- return promise

    return p
end

local Queued = {}

function DarkLib.Images.QueueLoadMaterials(...)
    local _images = {...}
    local images = {}
    -- fast foreach to check if they are already cached
    local ImgurCache = DarkLib.Images._ImgurCache

    for k, data in ipairs(_images) do
        if ImgurCache[data.image] ~= nil then
            DarkLib:LogDebug("Already Cached #" .. data.image)
            data.func(ImgurCache[data.image])
        else
            table.insert(images, data)
        end
    end

    DarkLib.AsyncForEach(images, function(k, data, done)
        if isstring(data) then
            _str = data

            data = {
                image = _str
            }
        end

        -- already Queued
        if Queued[data.image] then
            done()

            return
        end

        Queued[data.image] = true

        DarkLib.Images.LoadMaterial(data.image, function(result)
            if result and not result:IsError() then
                if isfunction(data.func) then
                    data.func(result)
                end

                DarkLib:LogDebug("[QUEUE] Loaded Image " .. data.image)
                Queued[data.image] = nil
                timer.Simple(0.1, done)
            else
                DarkLib:LogError("Could not load Imgur image " .. data.image)
                done()
                Queued[data.image] = nil
            end
        end)
    end, function() end)
end