--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
DarkLib.Utils = {}
local string_gsub, string_sub = string.gsub, string.sub
local string_gmatch, string_Replace = string.gmatch, string.Replace
local assert = assert
local table = table
local ipairs = ipairs
local pairs = pairs
local file = file
local CurTime = CurTime
local Lerp = Lerp
local math_Round = math.Round
local ThinkQueue = {}
local f, t = {}, {} -- Table creation may take some time 
local devCvar = GetConVar("developer") -- Easy way to detect Devmode

--[[
    Utility stuff
--]]
local function makePrefix(tbl, prefix, prefixColor)
    table.insert(tbl, 1, Color(200, 200, 200))
    table.insert(tbl, 1, prefix or "")
    table.insert(tbl, 1, prefixColor or Color(100, 255, 100))

    return tbl
end

--[[
    Can be used to check `var` with the `check` function.
    Returns the default one if not valid.
--]]
function DarkLib.ValidateVar(var, check, default)
    assert(isfunction(check), "check needs to be an function!")

    return check(var) and var or default
end

function DarkLib.LoadDirectory(directory, func)
    local files, _ = file.Find(directory .. "*.lua", "LUA")

    for i = 1, #files do
        local filename = files[i]
        DarkLib:LogDebug("Loading " .. directory .. filename)
        func(directory .. filename)
    end

    return files
end

function DarkLib.LoadAddonDirectory(dir, blacklist)
    local fil, fol = file.Find(dir .. "/*", "LUA")
    local blacklist = blacklist or {}

    for k, v in ipairs(fol) do
        if blacklist[v] then continue end
        DarkLib.LoadAddonDirectory(dir .. "/" .. v)
    end

    for k, v in ipairs(fil) do
        if blacklist[v] then continue end
        local dirs = dir .. "/" .. v
        DarkLib:LogDebug("Loading " .. dirs)

        if v:StartWith("cl_") then
            DarkLib.IncludeCL(dirs)
        elseif v:StartWith("sh_") then
            DarkLib.IncludeSH(dirs)
        else
            DarkLib.IncludeSV(dirs)
        end
    end
end

function DarkLib.IsDeveloperMode()
    return DarkLib.Debug:GetBool() or devCvar:GetInt() > 0
end

-- Original function by Aritz Cracker
function DarkLib.PlaceHolder(str, placeholders)
    local tab
    local tries = 0
    repeat
        tab = {string_gmatch(str, "(%%([^%%]*)%%)")()}
        tries = tries + 1

        for k, v in pairs(placeholders) do
            if tab[2] == k then
                str = string_Replace(str, tab[1], v)
                break
            end
        end

        if tries > 100 then return str end
    until (not tab[1])

    return str
end

-- Try to make the string at least amount of chars long (adds "char" until its enough)
function DarkLib.MinString(string, amount, char)
    if #string < (amount or 0) then
        for i = 1, (amount or 0) - #string do
            string = string .. (char or " ")
        end
    end

    return string
end

-- Will cut the string after amount of chars and add dots if enabled
function DarkLib.MaxString(string, amount, dots)
    local str = ""

    for i = 1, amount do
        str = str .. string[i]
    end

    if dots then
        str = str .. dots
    end

    return str
end

-- Get the current lua file name
function DarkLib.CurrentLuaFile()
    local source = debug.getinfo(2, "S").source

    if string_sub(source, 1, 1) == "@" then
        source = string_gsub(string_sub(source, 2), "addons.+lua.", "")
    end

    return source
end

-- Reloads the current files
function DarkLib.ReloadCurrentLuaFile()
    local fileName = DarkLib.CurrentLuaFile()

    -- Do it next 'tick'
    timer.Simple(0, function()
        include(fileName)
    end)
end

-- Merges tbl2 into tbl and returns it
function DarkLib.RecursiveTableMerge(tbl, tbl2)
    tbl = tbl or {}
    if not tbl2 then return tbl end

    for key, value in pairs(tbl2) do
        if istable(value) and istable(tbl[key]) then
            tbl[key] = DarkLib.RecursiveTableMerge(tbl[key], value)
        else
            tbl[key] = value
        end
    end

    return tbl
end

-- More optimised way of sending (larger) tables via net messages
-- Please note that sending tables should be avoided tho
-- And this is still limited by the buffer limit of 64k
function DarkLib.WriteTable(tbl_data)
    local json = util.TableToJSON(tbl_data)
    local data = util.Compress(json)
    net.WriteUInt(#data, 32)
    net.WriteData(data, #data)
end

-- Can read the table send with DarkLib.WriteTable
function DarkLib.ReadTable()
    local len = net.ReadUInt(32)
    local data = net.ReadData(len)

    return util.JSONToTable(util.Decompress(data))
end

-- Returns the addon informations used by DarkLib
function DarkLib.GetAddon(name)
    for i, k in ipairs(DarkLib.Addons) do
        if k.name == name then return k end
    end

    return false
end

-- Can be used to make the color darker or change the alpha
function DarkLib.ModifyColor(color, factor, alpha)
    local r, g, b, a = color.r, color.g, color.b, color.a
    if a and a ~= 255 then return Color(r * factor, g * factor, b * factor, alpha or (a * factor or nil)) end

    return Color(r * factor, g * factor, b * factor, alpha)
end

-- Lerps Color(from) to Color(to)
-- I tried to optimize it a bit to it will only be calculated when required
function DarkLib.LerpColor(frac, from, to)
    f.a, t.a = nil, nil -- They are not always used
    f.r, f.g, f.b, f.a = math_Round(from.r, 3), math_Round(from.g, 3), math_Round(from.b, 3), from.a and math_Round(from.a, 3)
    t.r, t.g, t.b, t.a = math_Round(to.r, 3), math_Round(to.g, 3), math_Round(to.b, 3), to.a and math_Round(to.a, 3)
    if f.r == t.r and f.g == t.g and f.b == t.b and f.a == t.a then return to end

    return Color(Lerp(frac, from.r, to.r), Lerp(frac, from.g, to.g), Lerp(frac, from.b, to.b), Lerp(frac, from.a, to.a))
end

-- Same as above but only lerps the Alpha
function DarkLib.LerpColorAlpha(frac, from, to)
    if math_Round(from.a, 3) == math_Round(to.a, 3) then return to end

    return Color(to.r, to.g, to.b, Lerp(frac, from.a, to.a))
end

-- Can be used to share a workload on multiple ticks
-- Same as "for i,k in" but instead of running them instantly it will be handled
-- in the next tick
function DarkLib.AsyncForEach(tab, func, done)
    local total = 0

    for k, v in pairs(tab) do
        total = total + 1
    end

    if (total == 0) then
        timer.Simple(0, done)
    else
        table.insert(ThinkQueue, {tab, func, done, total})
    end
end

-- Checks if the player is looking at (the direction of) targetVec
function DarkLib.IsLookingAt(ply, targetVec, float)
    return ply:GetAimVector():Dot((targetVec - ply:GetPos()):GetNormalized()) > (float or 0.9)
end

--[[
    local switch, case, default = DarkLib.Utils.switch, DarkLib.Utils.case, DarkLib.Utils.default

    switch(action, case(1, function()
        print("one")
    end), case(2, function()
        print("two")
    end), case(3, function()
        print("three")
    end), default(function()
        print("default")
    end))

]]
function DarkLib.Utils.switch(n, ...)
    local ran = false
    local cases = {...}

    for i = 1, #cases do
        v = cases[i]

        if v[1] == n or v[1] == nil or ran then
            local stuff = v[2]()
            if stuff ~= nil then return stuff end
            ran = true
        end
    end
end

function DarkLib.Utils.case(n, f)
    return {n, f}
end

function DarkLib.Utils.default(f)
    return {nil, f}
end

--[[
    Log functions
    TODO: this should be done better later
--]]
local ColorGrey = Color(210, 218, 226)
local ColorGreen = Color(5, 196, 107)

function DarkLib:AddLogger(addon)
    if not addon then
        addon = self
    end

    function addon:_Log(stuff)
        table.insert(stuff, 1, prefix or "[" .. (self.LogName or "DarkLib") .. "] ")
        table.insert(stuff, 1, prefixColor or Color(100, 255, 100))
        table.insert(stuff, "\n")
        MsgC(unpack(stuff))
    end

    function addon:Log(...)
        stuff = {...}
        makePrefix(stuff)
        self:_Log(stuff)
    end

    function addon:LogDebug(...)
        if not self.Debug:GetBool() then return end
        stuff = {...}
        makePrefix(stuff, "[DEBUG] ")
        self:_Log(stuff)
    end

    function addon:LogError(...)
        stuff = {...}
        makePrefix(stuff, "[ERROR] ", Color(255, 0, 0))
        self:_Log(stuff)

        if self.Debug:GetBool() then
            debug.Trace()
        end
    end

    function addon:LogWarn(...)
        stuff = {...}
        makePrefix(stuff, "[WARN] ", Color(255, 255, 0))
        self:_Log(stuff)
    end

    function addon:LogNotice(...)
        stuff = {...}
        makePrefix(stuff, "[NOTICE] ", Color(0, 255, 255))
        self:_Log(stuff)
    end

    function addon:LogPrefix(prefixColor, prefix, ...)
        stuff = {...}
        makePrefix(stuff, prefix or " ", prefixColor or Color(0, 255, 255))
        self:_Log(stuff)
    end

    function addon:MsgPly(ply, msg, printmode)
        if isentity(ply) and ply:IsPlayer() then
            if printmode and printmode ~= HUD_PRINTTALK then
                self:LogNotice("MsgPly: printmode will be deprecated in the future.")
                ply:PrintMessage(printmode, "[" .. (self.LogName or "DarkLib") .. "] " .. msg)
            else
                DarkLib.MsgPly(ply, ColorGrey, "[", self.Color or ColorGreen, self.LogName or "DarkLib", ColorGrey, "] " .. msg)
            end
        else
            self:Log(msg)
        end
    end

    return addon
end

-- Run these on itself
DarkLib:AddLogger()
local lastWork = CurTime()

-- Only usefull to share some work in multiple ticks
-- used by DarkLib.AsyncForEach
hook.Add("Think", "DarkLib Worker", function()
    if (lastWork + 0.1) > CurTime() then return end
    if #ThinkQueue < 1 then return end
    local QueueItem = table.remove(ThinkQueue, 1)
    local tab = QueueItem[1]
    local func = QueueItem[2]
    local done = QueueItem[3]
    local total = QueueItem[4]
    local progress = 0

    for k, v in pairs(tab) do
        func(k, v, function()
            progress = progress + 1

            if (progress == total) then
                timer.Simple(0, done)
            end
        end)
    end

    lastWork = CurTime()
end)