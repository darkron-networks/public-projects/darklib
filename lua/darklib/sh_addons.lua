--[[
    The most of this code is made by Aritz Cracker.
    https://github.com/ARitz-Cracker/arcbank/ (Apache License 2.0)
--]]
DarkLib.Addons = {}
local installedAddons = {}
local loadedaddons = {}
-- Register ourself
local len = 1

DarkLib.Addons[len] = {
    tabname = "DarkLib",
    filename = "darklib",
    name = "DarkLib",
    depends = {}
}

installedAddons["darklib"] = true
--[[
    Getting all Addons using this library
    Section 1: Initializing
--]]
local addons, _ = file.Find("darklib_addons/*.lua", "LUA")

for i = 1, #addons do
    addon_file = addons[i]
    AddCSLuaFile("darklib_addons/" .. addon_file)
    -- Creating addon object
    local tName, aName, dList = include("darklib_addons/" .. addon_file)

    if not isstring(aName) then
        DarkLib:LogError("Addon " .. addon_file .. " Is Invalid.")
        continue
    end

    local addon = {
        name = aName,
        filename = string.sub(addon_file, 1, -5),
        tabname = tName,
        depends = dList or {}
    }

    -- Add it to the List
    len = len + 1
    DarkLib.Addons[len] = addon
    installedAddons[addon.filename] = true
    DarkLib:LogDebug("Found addon: ", addon.name, #(addon.depends or {}) > 0 and ", depends on: " .. table.concat(addon.depends, ",") or ".")
end

--[[
    Filter out invalid addons
    Section 2: Filter out faulty addons
--]]
local i = 1

while i <= len do
    local valid = true
    local addon = DarkLib.Addons[i]

    for ii = 1, #addon.depends do
        if not installedAddons[addon.depends[ii]] then
            table.remove(DarkLib.Addons, i)
            len = len - 1
            i = 1
            -- Now any other addon that depended on this cannot be loaded
            installedAddons[addon.filename] = false
            valid = false
            DarkLib:LogError("WARNING! " .. addon.name .. " has an missing dependency! (" .. addon.depends[ii] .. ") This addon can not be loaded!")
            break
        end

        if not loadedaddons[addon.depends[ii]] then
            local addon = table.remove(DarkLib.Addons, i)
            i = i - 1
            DarkLib.Addons[len] = addon
            valid = false
            break
        end
    end

    if valid then
        loadedaddons[addon.filename] = true
    end

    i = i + 1
end

--[[
    Loading all valid addons
    Section 3: Loading Addons
--]]
local function loadIfExists(fileName, func)
    if file.Exists(fileName, "LUA") then
        func(fileName)
        DarkLib:LogDebug("Loaded Config: " .. fileName)
    end
end

for i = 2, len do
    local addon = DarkLib.Addons[i]
    local dir = addon.filename
    local aTable = addon.tabname and _G[addon.tabname] or nil

    -- We need this as soon as possible tho
    if istable(aTable) then
        aTable.TableName = addon.tabname
        aTable.FileName = addon.filename
        aTable.LogName = aTable.LogName or addon.name
        DarkLib:AddLogger(aTable)
        -- Setup Convars 
        aTable.Debug = CreateConVar(addon.filename .. "_debug", "0", FCVAR_CLIENTCMD_CAN_EXECUTE, "Change Debugmode for " .. addon.name, 0, 1)
        DarkLib:LogDebug("Created Debug Convar '" .. addon.filename .. "_debug'.")
    end

    DarkLib:LogNotice("Loading " .. addon.name .. "...")
    local LoaderSettings = aTable and aTable.LoaderSettings or {}

    if LoaderSettings and LoaderSettings.UseSimpleLoader then
        -- Simple way to load them (load files recursively depending on cl_ or sh_ in file name, everything else will be server)
        DarkLib.LoadAddonDirectory(dir, LoaderSettings.Blacklist or {})
    else
        -- Loading SHARED files 
        loadIfExists(dir .. "/sh_config.lua", DarkLib.IncludeSH)
        DarkLib.LoadDirectory(dir .. "/shared/", DarkLib.IncludeSH)

        if SERVER then
            -- Loading SERVER files
            loadIfExists(dir .. "/sv_config.lua", DarkLib.IncludeSV)
            DarkLib.LoadDirectory(dir .. "/server/", DarkLib.IncludeSV)
        end

        -- Loading CLIENT stuff
        DarkLib.LoadDirectory(dir .. "/client/", DarkLib.IncludeCL)
    end
end

--[[
    Setting up Addons and "Start" them
    Section 4: Setup Addons (if required)
--]]
for i = 2, len do
    local addon = DarkLib.Addons[i]
    local aTable = _G[addon.tabname]
    DarkLib:LogDebug("Setup addon " .. addon.name)

    if type(aTable) == "table" and type(aTable.Load) == "function" then
        aTable.Load()
    end

    DarkLib:LogNotice("Loaded Addon " .. addon.name, Color(150, 150, 150), addon.tabname and aTable.Version and (" (" .. aTable.Version .. ")") or "")
end