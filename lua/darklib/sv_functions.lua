--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
--[[---------------------------------------
    A simple wrapper for the mysqli wrapper
    Usage:

    DarkLibSQL.initialize({
        EnableMySQL = false, --             Bool   - set to true to use MySQL, false for SQLite
        Host = "localhost", --              String - database hostname
        Username = "root", --               String - database username
        Password = "", --                   String - database password (keep away from clients!)
        Database_name = "local_darkrp", --  String - name of the database
        Database_port = nil, --             Number - connection port (3306 by default)
        Preferred_module = nil, --          String - Preferred module, case sensitive, must be either "mysqloo" or "tmysql4"
        MultiStatements = nil --            Bool   - Only available in tmysql4: allow multiple SQL statements per query
    })

    DarkLib:InvokeSQL(DarkLibSQL, "SELECT 5+5,2.229484e+9", "test"):next(function(result)
        if istable(result) then
            PrintTable(result)
        else
            print('Result', tostring(result))
        end
    end, function(err)
        print('Error', err)
    end)
-----------------------------------------]]
function DarkLib:InvokeSQL(conn, queryStr, identifer)
    local promise = DarkLib.Promise.new()
    local debug = self.DEBUG

    if not conn then
        self:LogDebug("[Database] Invalid connection!")

        return promise:reject("Invalid Connection")
    end

    if (not isstring(queryStr)) then
        self:LogError("Trying to query something that is not a string. type: " .. type(str) .. ", content: " .. tostring(str))

        return promise:reject("Not a string")
    end

    local sqlStr = conn.isMySQL() and "[MySQL] " or "[SQLite] "

    local successDetour = function(result)
        self:LogDebug(sqlStr .. identifer .. ": Success!")
    end

    local _successFunc = function(result)
        successDetour(result)
        promise:resolve(result)
    end

    local errDetour = function(err)
        if (debug) then
            self:LogError(sqlStr .. identifer .. ": Error! " .. err)
        end
    end

    local _errFunc = function(err)
        errDetour(err)
        promise:reject(err)
    end

    conn.query(queryStr, _successFunc, _errFunc)

    return promise
end

--[[
    Registers a command handler with `command` as root command name for `addon`
]]
local MinString = DarkLib.MinString

function DarkLib.AddonConcommand(addon, command)
    if not _G[addon] then
        DarkLib:LogError("Tried to register a command handler for " .. addon .. " which doesnt exists!")

        return
    end

    _G[addon].Commands = _G[addon].Commands or {}
    _G[addon]._Command = command

    _G[addon].Commands["help"] = {
        func = function(ply, args)
            if not (_G[addon] and _G[addon].Loaded) then
                _G[addon]:MsgPly(ply, addon .. " is not Loaded.", HUD_PRINTCONSOLE)

                return
            end

            if args[1] then
                if _G[addon].Commands[args[1]] then
                    _G[addon]:MsgPly(ply, args[1] .. tostring(_G[addon].Commands[args[1]].usage) .. " - " .. tostring(_G[addon].Commands[args[1]].description), HUD_PRINTCONSOLE)
                else
                    _G[addon]:MsgPly(ply, "No such command as " .. tostring(args[1]), HUD_PRINTCONSOLE)
                end
            else
                local commandlist = [[>>> Command List
Syntax:
<name(type)> = required
[name(type)] = optional

List of commands:]]

                for name, _ in SortedPairs(_G[addon].Commands) do
                    local setting = _G[addon].Commands[name]

                    if not setting.hidden then
                        commandlist = commandlist .. "\n" .. MinString("> " .. name .. setting.usage, 50) .. " - " .. setting.description
                    end
                end

                _G[addon]:MsgPly(ply, commandlist, HUD_PRINTCONSOLE)
            end
        end,
        usage = " [command(string)]",
        description = "Gives you a description of every command.",
        adminonly = false,
        hidden = false
    }

    concommand.Add(command, function(ply, cmd, args)
        local comm = args[1]
        table.remove(args, 1)

        if _G[addon].Commands[comm] then
            local AdminGroups = _G[addon].Settings and _G[addon].Settings.admins or false

            if _G[addon].Commands[comm].admin and IsValid(ply) and not (AdminGroups and table.HasValue(AdminGroups, string.lower(ply:GetUserGroup())) or ply:IsSuperAdmin()) then
                _G[addon]:MsgPly(ply, "You dont have the permission to run this command.")
                _G[addon]:LogDebug(ply:Nick() .. " (" .. ply:SteamID() .. ") has no access to " .. comm)

                return
            end

            if _G[addon].Commands[comm].notice and IsValid(ply) then
                local logstr = ply:Nick() .. " (" .. ply:SteamID() .. ") used the command: " .. comm

                for i = 1, #args do
                    logstr = logstr .. " '" .. args[i] .. "'"
                end

                _G[addon]:LogNotice(logstr)
            end

            _G[addon].Commands[comm].func(ply, args)
        else
            _G[addon]:MsgPly(ply, "Invalid command '" .. tostring(comm or "?") .. "' Type '" .. command .. " help' for help.")
        end
    end)
end

-- Chat stuff
util.AddNetworkString("DarkLib:ChatMessage")

function DarkLib.MsgPly(ply, ...)
    local message = {...}
    local p = DarkLib.Net.Sender("DarkLib:ChatMessage")

    for _, part in ipairs(message) do
        local isColor = IsColor(part)
        p:WriteBool(isColor)

        if isColor then
            p:WriteColor(part)
        else
            p:WriteString(part)
        end
    end

    p:Send(ply)
end

-- Just a small prevention of future problems in case there are problems
hook.Add("PlayerInitialSpawn", "DarkLib.PlayerInit", function(ply)
    hook.Run("DarkLib.PlayerInitialised", ply)
end)