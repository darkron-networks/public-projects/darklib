--[[
    Copyright 2020 Justin Klüver (JustPlayer)
    Apache License 2.0
]]
local math_ceil = math.ceil
local ClearStencil, SetStencilEnable = render.ClearStencil, render.SetStencilEnable
local SetStencilCompareFunction, SetStencilPassOperation = render.SetStencilCompareFunction, render.SetStencilPassOperation
local SetStencilFailOperation, SetStencilZFailOperation = render.SetStencilFailOperation, render.SetStencilZFailOperation
local SetStencilWriteMask, SetStencilTestMask = render.SetStencilWriteMask, render.SetStencilTestMask
local SetStencilReferenceValue, SetStencilReferenceValue = render.SetStencilReferenceValue, render.SetStencilReferenceValue
local OverrideColorWriteEnable, SetStencilCompareFunction = render.OverrideColorWriteEnable, render.SetStencilCompareFunction
--[[
    Utility stuff
--]]
local ScrW, ScrH = ScrW(), ScrH()

-- Same as default ScreenScale, but is based on 1080p 
-- and ceil the output
function DarkLib.Scale(base)
    return math_ceil(ScrH * (base / 1080))
end

-- For lazy people who dont want to calculate the center of something on the own
function DarkLib.Center(base)
    return base / 2 - (base / 2)
end

-- Font Helpers
local fontDefault = {
    font = "Roboto",
    extended = false,
    size = DarkLib.Scale(18),
    weight = 600
}

function DarkLib.CreateFontEx(name, data)
    DarkLib:LogDebug("Created Font: " .. name)
    surface.CreateFont("DarkUI." .. name, DarkLib.RecursiveTableMerge(fontDefault, data))
end

function DarkLib.CreateFont(name, size, font)
    DarkLib.CreateFontEx(name, {
        size = DarkLib.Scale(size or 18),
        font = font
    })
end

-- Chat stuff
DarkLib.Net.Receiver("DarkLib:ChatMessage", function(packet)
    local message = {}

    while packet:HasDataLeft() do
        table.insert(message, packet:ReadBool() and packet:ReadColor() or packet:ReadString())
    end

    chat.AddText(unpack(message))
end)

-- Stencil helper 
-- Simple way to use Stencils, you should learn stencils before using it tho
function DarkLib.StartCutOut(bufferFunc, drawFunc)
    if not bufferFunc then return end -- to prevent errors
    -- Reset
    ClearStencil()
    SetStencilEnable(true)
    SetStencilCompareFunction(STENCIL_ALWAYS)
    SetStencilPassOperation(STENCIL_REPLACE)
    SetStencilFailOperation(STENCIL_KEEP)
    SetStencilZFailOperation(STENCIL_KEEP)
    SetStencilWriteMask(1)
    SetStencilTestMask(1)
    SetStencilReferenceValue(1)
    OverrideColorWriteEnable(true, false)
    -- Draw Buffer
    bufferFunc()
    -- Set the compare to equal the buffer value
    OverrideColorWriteEnable(false, false)
    SetStencilCompareFunction(STENCIL_EQUAL)
    -- Only do it when its set
    if not drawFunc then return end
    -- Draw the stuff
    drawFunc()
    -- Disable Stencil
    SetStencilEnable(false)
end

-- Will close the stencil
-- I recommend to just call SetStencilEnable yourself or use the drawFunc in StartCutOut
function DarkLib.StopCutOut()
    SetStencilEnable(false)
end

hook.Add("OnScreenSizeChanged", "DARKLIB:ResetFonts", function()
    ScrW, ScrH = ScrW(), ScrH()
end)

--[[
    Default Materials
--]]
DarkLib.Materials = {}
DarkLib.Materials.Cross = Material("error")
DarkLib.Materials.Loading = Material("error")
DarkLib.ScreenScale = DarkLib.Scale -- Backwards
--[[
    Quality overwrites
--]]
CreateClientConVar("darklib_shadows", 1, true, false, "Enables Shadows for some elements")